//
//  AST.h
//  BabyCC
//
//  Created by Patricio Bulic on 03/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef AST_h
#define AST_h

#include <stdio.h>
#include "list.h"





typedef enum {
    AST_TYPE_SPECIFIER,
    AST_CONSTANT,
    AST_IDENTIFIER,
    AST_UNARY_OP,
    AST_BINARY_OP,
    AST_RELATIONAL_OP,
    AST_SHIFT_OP,
    AST_EQAULITY_OP,
    AST_ASSIGNMENT_EX,
    AST_INIT_DECLARATOR,
    AST_DECLARATION_STATEMENT,
    AST_DECLARATION_LIST,
    AST_PARAMETER_DECLARATION,
    AST_PARAMETER_LIST,
    AST_ARGUMENT_LIST,
    AST_JUMP_STATEMENT,
    AST_EXPRESSION_STATEMENT,
    AST_ITERATION_STATEMENT,
    AST_SELECTION_STATEMENT,
    AST_BLOCK,
    AST_COMPOUND_STATEMENT,
    AST_FUNCTION_DEFINITION,
    AST_FUNCTION_DECLARATOR,
    AST_FUNCTION_CALL,
    AST_TRANSLATIONAL_UNIT
} ASTNodeType;


typedef enum {
    //BE_LEFT_ONLY,         // used to designate expression without operation and with left expression only
    BE_ADDITION,
    BE_SUBTRACTION,
    BE_MULTIPLICATION,
    BE_DIVISION,
    BE_MODULUS,
    BE_BIN_AND,
    BE_BIN_XOR,
    BE_BIN_OR,
    BE_LOG_AND,
    BE_LOG_OR
} ASTBinaryExpressionType;



typedef enum {
    UE_PLUS,
    UE_MINUS,
    UE_ADDRESS,
    UE_DEREFERENCE,
    UE_BITWISENEG,
    UE_LOGNEG
} ASTUnaryExpressionType;


typedef enum {
    RE_LESS_THAN,
    RE_LESS_THAN_OR_EQUAL,
    RE_GREATER_THAN,
    RE_GREATER_THAN_OR_EQUAL
} ASTRelationalExpressionType;


typedef enum {
    SE_LEFT,
    SE_RIGHT
} ASTShiftExpressionType;

typedef enum {
    EE_EQUAL,
    EE_NOT_EQUAL
} ASTEqualityExpressionType;


typedef enum {
    NOT_USED_FOR_NOW
} ASTExpressionType;

typedef enum {
    AE_ASSIGN,
    AE_MUL_ASSIGN,
    AE_DIV_ASSIGN,
    AE_MOD_ASSIGN,
    AE_ADD_ASSIGN,
    AE_SUB_ASSIGN,
    AE_LEFT_ASSIGN,
    AE_RIGHT_ASSIGN,
    AE_AND_ASSIGN,
    AE_XOR_ASSIGN,
    AE_OR_ASSIGN
} ASTAssignmentExpressionType;


typedef enum {
    JS_GOTO,
    JS_CONTINUE,
    JS_BREAK,
    JS_RETURN,
    JS_RETURN_EXP
} ASTJumpStatementType;

typedef enum {      // this is actually redundant, but used anyway
    ES_EXPRESSION
} ASTExpressionStatementType;


typedef enum {
    IS_WHILE,
    IS_DO,
    IS_FOR
} ASTIterationStatementType;


typedef enum {
    SS_IF,
    SS_IFELSE,
    SS_SWITCH
} ASTSelectionStatementType;


typedef enum {
    DS_GLOBAL,      // global declaration
    DS_LOCAL,       // local declaration
    DS_ARGUMENTS    // function argument declaration
} ASTDeclarationStatementType;

typedef enum {
    S_LABELED,
    S_COMPOUND,
    S_EXPRESSION,
    S_SELECTION,
    S_ITERATION,
    S_JUMP
} ASTStatementType;

typedef enum {
    TS_VOID,
    TS_CHAR,
    TS_SHORT,
    TS_INT,
    TS_LONG,
    TS_FLOAT,
    TS_DOUBLE,
    TS_SIGNED,
    TS_UNSIGNED,
    TS_BOOL,
    TS_COMPLEX,
    TS_IMAGINARY
} ASTTypeSpecifierType;

typedef enum {
    ID_NOINIT,
    ID_INIT
} ASTInitDeclaratorType;


typedef enum {
    FD_NOARGS,
    FD_ARGS
} ASTFunctionDeclaratorType;





// We need to define a typedef ASTnode before you can reference it in e.g. BinaryExpression.
// Later, we declare a real structure.
struct ASTnode;
typedef struct ASTnode ASTnode;


typedef struct ASTTypeSpecifier {
    ASTTypeSpecifierType type;
    ASTnode *specifier; /*TODO: struct specifier, enum specifier*/
} ASTTypeSpecifier;


typedef struct ASTConstant {
    int value;
} ASTConstant;

typedef struct ASTIdentifier {
    // TODO: once we have other types, we will need to store type here.
    //IdentifierType identifier_type;
    char *identifier_name;
} ASTIdentifier;



typedef struct ASTUnaryExpression {
    ASTUnaryExpressionType type;
    ASTnode *right;
} ASTUnaryExpression;


typedef struct ASTBinaryExpression {
    ASTBinaryExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTBinaryExpression;


typedef struct ASTRelationalExpression {
    ASTRelationalExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTRelationalExpression;


typedef struct ASTShiftExpression {
    ASTShiftExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTShiftExpression;

typedef struct ASTEqualityExpression {
    ASTEqualityExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTEqualityExpression;


typedef struct ASTAssignmentExpression {
    ASTAssignmentExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTAssignmentExpression;


typedef struct ASTExpression {
    ASTExpressionType type;
    ASTnode *left;
    ASTnode *right;
} ASTExpression;


typedef struct ASTFunctionDeclarator {
    ASTFunctionDeclaratorType type;
    ASTnode *func_identifier;
    ASTnode *parameter_type_list;
} ASTFunctionDeclarator;


typedef struct ASTFunctionCall {
    ASTFunctionDeclaratorType type;
    ASTnode *name;
    ASTnode *argument_list;
} ASTFunctionCall;


typedef struct ASTInitDeclarator {
    ASTInitDeclaratorType type;
    ASTnode *declarator;
    ASTnode *initializer;
} ASTInitDeclarator;


typedef struct ASTJumpStatement {
    ASTJumpStatementType type;
    ASTnode *expression;
} ASTJumpStatement;


typedef struct ASTExpressionStatement {
    ASTExpressionStatementType type;
    ASTnode *expression;
} ASTExpressionStatement;

typedef struct ASTSelectionStatement {
    ASTSelectionStatementType type;
    ASTnode *condition;
    ASTnode *true_statement;
    ASTnode *false_statement;
} ASTSelectionStatement;

typedef struct ASTIterationStatement {
    ASTIterationStatementType type;
    ASTnode *condition;
    ASTnode *statement;
    ASTnode *forinit;
    ASTnode *forupdate;
} ASTIterationStatement;


typedef struct ASTStatement {
    ASTStatementType type;
    ASTnode *statement;
} ASTStatement;


typedef struct ASTCompoundStatement {
    //ASTCompoundStatementType type;
    ASTnode *block;
} ASTCompoundStatement;



typedef struct ASTDeclarationStatement {
    ASTDeclarationStatementType type;   //this would by DS_GLOBAL or DS_LOCAL
    ASTnode *specifier;
    ASTnode *declarator_list; /*TODO: this should be list*/
} ASTDeclarationStatement;


typedef struct ASTParameterDeclaration {    // for function arguments declaraton
    ASTDeclarationStatementType type;       // this would by DS_ARGUMENTS
    ASTnode *specifier;
    ASTnode *declarator;
} ASTParameterDeclaration;


typedef struct ASTDeclarationList {
    List *declaration_statements;
} ASTDeclarationList;

typedef struct ASTParametertList {
    List *parameter_declarations;
} ASTParametertList;


typedef struct ASTArgumentList {
    List *arguments;
} ASTArgumentList;


typedef struct ASTBlock {
    List *statements;
} ASTBlock;


typedef struct ASTFunctionDefinition {
    ASTnode *declaration_specifiers;
    ASTnode *declarator;
    ASTnode *compound_statement;
} ASTFunctionDefinition;


typedef struct ASTTranslationalUnit {
    List *ext_declarations;   // these nodes are of type AST_FUNCTION_DEFINITION or AST_DECLARATION_STATEMENT
} ASTTranslationalUnit;


struct ASTnode {
    ASTNodeType type;
    union {
        ASTTypeSpecifier *type_specifier;
        ASTConstant *constant;
        ASTIdentifier *identifier;
        ASTUnaryExpression *unary_expression;
        ASTBinaryExpression *binary_expression;
        ASTRelationalExpression *relational_expression;
        ASTShiftExpression *shift_expression;
        ASTEqualityExpression *equality_expression;
        ASTAssignmentExpression *assignment_expression;
        ASTExpression *expression;
        
        ASTInitDeclarator *init_declarator;
        
        ASTDeclarationStatement *declaration_statement;
        ASTDeclarationList *declaration_list;
        ASTJumpStatement *jump_statement;
        ASTExpressionStatement *expression_statement;
        ASTSelectionStatement *selection_statement;
        ASTIterationStatement *iteration_statement;
        ASTStatement *statement;
        ASTBlock *block;
        ASTCompoundStatement *compound_statement;
        
        ASTParameterDeclaration *parameter_declaration;
        ASTParametertList *parameter_list;
        ASTArgumentList *argument_list;
        
        ASTFunctionDefinition *function_definition;
        ASTFunctionDeclarator *function_declarator;
        ASTFunctionCall *function_call;
        
        ASTTranslationalUnit *translational_unit;
    };
};


ASTnode *ast_constant_new(int value);
ASTnode *ast_identifier_new(char *identifier_name);
ASTnode *ast_unary_new(ASTnode *right, ASTUnaryExpressionType type);
ASTnode *ast_binary_new(ASTnode *left, ASTnode *right, ASTBinaryExpressionType type);
ASTnode *ast_relational_new(ASTnode *left, ASTnode *right, ASTRelationalExpressionType type);
ASTnode *ast_shift_new(ASTnode *left, ASTnode *right, ASTShiftExpressionType type);
ASTnode *ast_equality_new(ASTnode *left, ASTnode *right, ASTEqualityExpressionType type);
ASTnode *ast_assignment_new(ASTnode *left, ASTnode *right, ASTAssignmentExpressionType type);

ASTnode *ast_type_spec_new(ASTTypeSpecifierType type);
ASTnode *ast_init_declarator_new(ASTnode *declarator, ASTnode *initializer, ASTInitDeclaratorType type);

ASTnode *ast_jump_statement_new(ASTnode *expression, ASTJumpStatementType type);
ASTnode *ast_declaration_statement_new(ASTnode *specifier, ASTnode *declarator_list, ASTDeclarationStatementType type);
ASTnode *ast_declaration_list_new(List *declaration_statements);
ASTnode *ast_parameter_declaration_new(ASTnode *specifier, ASTnode *declarator, ASTDeclarationStatementType type);
ASTnode *ast_parameter_list_new(List *parameter_declarations);
ASTnode *ast_argument_list_new(List *arguments);

ASTnode *ast_expression_statement_new(ASTnode *expression, ASTExpressionStatementType type);
ASTnode *ast_selection_statement_new(ASTnode *condition,ASTnode *true_statement,ASTnode *false_statement,ASTSelectionStatementType type);
ASTnode *ast_iteration_statement_new(ASTnode *condition,ASTnode *statement, ASTnode *forinit,ASTnode *forupdate,ASTIterationStatementType type);


ASTnode *ast_block_new(List *statements);
ASTnode *ast_compound_statement_new(ASTnode *block);
ASTnode *ast_function_definition_new(ASTnode *declaration_specifiers,ASTnode *declarator,ASTnode *compound_statement);
ASTnode *ast_function_call_new(ASTnode *name,ASTnode *argument_list,ASTFunctionDeclaratorType type);
ASTnode *ast_function_declarator_new(ASTnode *name,ASTnode *parameter_type_list,ASTFunctionDeclaratorType type);

ASTnode *ast_translational_unit_new(List *ext_declarations);

void ast_list_free(List *ast_nodes);

char* AST_type_name(ASTnode *astnode);
void AST_free(ASTnode *astnode);
void AST_dump(FILE* out, ASTnode *astnode);



void help (ASTnode *declarator);


#endif /* AST_h */


