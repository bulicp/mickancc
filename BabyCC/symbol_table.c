//
//  symbol_table.c
//  BabyCC
//
//  Created by Patricio Bulic on 06/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//


#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include "symbol_table.h"



SymbolTable* symbol_table_new(){
    SymbolTable* st = malloc(sizeof(SymbolTable));
    
    st->length = 0;
    st->symbols = NULL;
    
    return st;
}



int symbol_table_length(SymbolTable* symtab){
    
    return symtab->length;
}


void symbol_table_free(SymbolTable* symtab){
    
    while (symtab->symbols != NULL) {
        Symbol* sym = symtab->symbols;
        symtab->symbols = symtab->symbols->next;
        free(sym);
    }
    
    free(symtab);
}



/*
* Insert a symbol at the beginning of the symbol table
*/
/* OL
void symbol_insert(SymbolTable* symtab, int scope, char* name, SymbolType type){
    Symbol* sym = malloc(sizeof(Symbol));
    
    sym->name = name;
    sym->scope = scope;
    sym->type = type;
    sym->next = symtab->symbols;
    
    symtab->length ++;
    symtab->symbols = sym;
}
 */

void symbol_insert(SymbolTable* symtab, Symbol *sym){
    
    sym->next = symtab->symbols;
    
    symtab->length ++;
    symtab->symbols = sym;
}


/*
 * Removes the symbol at the beginning of the symbol table
 */
void symbol_remove(SymbolTable* symtab){
    Symbol* sym = symtab->symbols;
    
    symtab->symbols = sym->next;
    symtab->length --;
    
    if (sym->type == ST_VARIABLE){
        free(sym->variable_symbol);
    }
    else if (sym->type == ST_FUNCTION){
        free(sym->function_symbol);
    }
    else if (sym->type == ST_LABEL){
        free(sym->label_symbol);
    }
    
    free(sym);
}


Symbol* symbol_get(SymbolTable* symtab, char* name){
//Symbol* symbol_get(SymbolTable* symtab, int scope, char* name){
    Symbol* sym = symtab->symbols;
    
    while (sym != NULL) {
        //if (sym->scope == scope){
            if (strcmp(sym->name, name) == 0) {
                return sym;
            }
        //}
        sym = sym->next;
    }
    
    return NULL;
}


bool symbol_lookup(SymbolTable* symtab, char* name) {
    Symbol* sym = symtab->symbols;
    
    while (sym != NULL) {
        //if (sym->scope == scope){
            if (strcmp(sym->name, name) == 0) {
                return true;
            }
        //}
        sym = sym->next;
    }
    
    return false;
}


bool symbol_lookup_in_scope(SymbolTable* symtab, int scope, char* name, STVariableScopeType var_scope) {
//bool symbol_lookup(SymbolTable* symtab, int scope, char* name) {
    Symbol* sym = symtab->symbols;
    //char* search_name = name;
    
    //printf("search_name: %s \n", name);
    while (sym != NULL) {
        if (sym->scope == scope){
            //printf("curent name in ST: %s, search_name: %s \n", sym->name, name);
            if (strcmp(sym->name, name) == 0 && sym->variable_symbol->var_scope == var_scope) {
                return true;
            }
        }
        sym = sym->next;
    }
    
    return false;
}


/*
 * Close a scope - remove all symbols in the scope from symtab
 */
void scope_close(SymbolTable* symtab, int scope){
    
    while (symtab->symbols->scope == scope) {
        symbol_remove(symtab);
    }
    
}

/*
 * Close a scope - remove all symbols in the scope from symtab
 */
int scope_length(SymbolTable* symtab, int scope){
    Symbol* sym = symtab->symbols;
    int length = 0;
    
    while (sym != NULL) {
        if (sym->scope == scope){
            length++;
        }
        sym = sym->next;
    }
    
    return length;
    
}




/*
 * Create new variable symbol
 */
Symbol* var_symbol_new(int scope, char* name, int stack_offset, int storage_reg, STTypeSpecifierType type, STVariableScopeType var_scope){
    VarSymbol* vs = malloc(sizeof(VarSymbol));
    vs->stack_offset = stack_offset;
    vs->storage_reg = storage_reg;
    vs->var_type = type;
    vs->var_scope = var_scope;
    
    Symbol* sym = malloc(sizeof(Symbol));
    sym->name = name;
    sym->scope = scope;
    sym->type = ST_VARIABLE;
    sym->variable_symbol = vs;
    
    return sym;
}


/*
 * Create new function symbol
 */
Symbol* func_symbol_new(int scope, char* name, STTypeSpecifierType type){
    FunctionSymbol* fs = malloc(sizeof(FunctionSymbol));
    fs->func_type = type;
    fs->var_scope = ST_GLOBAL;
    
    Symbol* sym = malloc(sizeof(Symbol));
    sym->name = name;
    sym->scope = scope;
    sym->type = ST_FUNCTION;
    sym->function_symbol = fs;
    
    return sym;
}


/*
 * Create new label symbol
 */
Symbol* label_symbol_new(int scope, char* name){
    LabelSymbol* ls = malloc(sizeof(LabelSymbol));
    
    Symbol* sym = malloc(sizeof(Symbol));
    sym->name = name;
    sym->scope = scope;
    sym->type = ST_LABEL;
    sym->label_symbol = ls;
    
    return sym;
}


