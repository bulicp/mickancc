//
//  list.h
//  BabyCC
//
//  Created by Patricio Bulic on 02/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef list_h
#define list_h


#define INITIAL_LIST_SIZE 32

typedef struct List {
    int size;
    void **items;
} List;



List *list_new(void);

int list_length(List *list);

void list_free(List *list);

void list_append(List *list, void *item);

void list_push(List *list, void *item);

void *list_pop(List *list);

void *list_get(List *list, int index);

void list_set(List *list, int index, void *value);

#endif /* list_h */
