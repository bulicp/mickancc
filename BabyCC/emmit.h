//
//  emmit.h
//  BabyCC
//
//  Created by Patricio Bulic on 04/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef emmit_h
#define emmit_h

#include <stdio.h>
#include "AST.h"
#include "tac.h"


void AST_parse(FILE* out, ASTnode *astnode, TAC **new_tac);
void dump_TAC_list(FILE* out, List* tac_list);
void dump_dataseg_TAC_list(FILE* out, List* tac_list);

#endif /* emmit_h */
