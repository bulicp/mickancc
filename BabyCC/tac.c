//
//  tac.c
//  BabyCC
//
//  Created by Patricio Bulic on 07/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//


#include "tac.h"


#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include "AST.h"
#include "list.h"
//#include "symbol_table.h"

extern List* tac_list_noemmit;




TAC *tac_entrypoint_new() {
    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_ENTRYPOINT;

    return tac;
}

TAC *tac_exitpoint_new() {
    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_EXITPOINT;

    return tac;
}


TAC *tac_stackpointer_new(int constant, TAC_STACKPOINTER_Type type) {
    TACStackPointer* tac_sp = malloc(sizeof(TACStackPointer));
    tac_sp->constant = constant;
    tac_sp->type = type;
    
    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_STACKPOINTER;
    tac->tac_stack_pointer = tac_sp;

    return tac;
}



TAC *tac_constant_new(int value, int tacidx) {
    TACConstant *tac_c = malloc(sizeof(TACConstant));
    tac_c->value = value;
    //tac_c->storage_reg = storage_reg;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_CONSTANT;
    tac->tacidx = tacidx;
    tac->tac_constant = tac_c;

    return tac;
}


TAC *tac_identifier_new(char* name, int storage_reg) {
    TACIdentifier *tac_i = malloc(sizeof(TACIdentifier));
    tac_i->name = name;
    tac_i->storage_reg = storage_reg;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_IDENTIFIER;
    tac->tac_identifier = tac_i;

    return tac;
}


TAC *tac_framepointer_access_new(int stack_offset, TAC* destination, TAC_FPACCESS_Type type) {
    TACFramePointerAccess *tac_fpa = malloc(sizeof(TACFramePointerAccess));
    tac_fpa->stack_offset = stack_offset;
    tac_fpa->type = type;
    tac_fpa->destination = destination;
    

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_FPACCESS;
    tac->tac_framepointer_access = tac_fpa;

    return tac;
}


TAC *tac_label_new(int labelidx, char* name) {
    TACLabel *tac_l = malloc(sizeof(TACLabel));
    tac_l->labelidx = labelidx;
    tac_l->name = name;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_LABEL;
    tac->labelidx = labelidx;
    tac->tac_label = tac_l;

    return tac;
}



TAC *tac_unary_new(TAC *source, TAC_BINOP_Type op, int tacidx) {
    TACBinary *tac_bin = malloc(sizeof(TACBinary));  // Unary expressions trsanslate to binary TACs!
    tac_bin->operation = op;
    tac_bin->source1 = source;
    tac_bin->source2 = NULL;

    TAC *tac = malloc(sizeof(TAC));
    tac_bin->destination = tac;  //destination points to itself
    tac->type = TAC_UNARY;
    tac->tacidx = tacidx;
    tac->tac_binary = tac_bin;

    return tac;
}


TAC *tac_stack_new(TAC *source, TAC_STACK_Type op) {
    TACStack *tac_stack = malloc(sizeof(TACStack));  // Unary expressions trsanslate to binary TACs!
    tac_stack->type = op;
    tac_stack->source = source;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_STACK;
    tac->tac_stack = tac_stack;

    return tac;
}


TAC *tac_binary_new(TAC *source1, TAC *source2, TAC_BINOP_Type op, int tacidx) {
    TACBinary *tac_bin = malloc(sizeof(TACBinary));
    tac_bin->operation = op;
    tac_bin->source1 = source1;
    tac_bin->source2 = source2;

    TAC *tac = malloc(sizeof(TAC));
    tac_bin->destination = tac;  //destination points to itself
    tac->type = TAC_BINARY;
    tac->tacidx = tacidx;
    tac->tac_binary = tac_bin;

    return tac;
}


TAC *tac_relational_new(TAC *source1, TAC *source2, TAC_RELOP_Type op, int tacidx) {
    TACRelational *tac_rel = malloc(sizeof(TACRelational));
    tac_rel->operation = op;
    tac_rel->source1 = source1;
    tac_rel->source2 = source2;

    TAC *tac = malloc(sizeof(TAC));
    tac_rel->destination = tac;  //destination points to itself
    tac->type = TAC_RELATIONAL;
    tac->tacidx = tacidx;
    tac->tac_relational = tac_rel;
    
    return tac;
}




TAC *tac_condbranch_new(TAC *source, TAC *label, TAC_CONDBRANCH_Type op, int tacidx) {
    TACCondBranch *tac_cb = malloc(sizeof(TACCondBranch));
    
    tac_cb->operation = op;
    tac_cb->source = source;
    tac_cb->branch_label = label;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_CONBRANCH;
    tac->tacidx = tacidx;
    tac->tac_condbranch = tac_cb;

    return tac;
}


TAC *tac_jump_new(TAC *label, int tacidx) {
    TACJump *tac_j = malloc(sizeof(TACJump));
    
    tac_j->branch_label = label;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_JUMP;
    tac->tacidx = tacidx;
    tac->tac_jump = tac_j;

    return tac;
}


TAC *tac_call_new(TAC *label, int tacidx) {
    TACCall *tac_c = malloc(sizeof(TACCall));
    
    tac_c->branch_label = label;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_CALL;
    tac->tacidx = tacidx;
    tac->tac_call = tac_c;

    return tac;
}



TAC *tac_globalvar_new(TAC *source) {
    TACGlobalVar *tac_gv = malloc(sizeof(TACGlobalVar));
    tac_gv->source = source;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_GLOBALVAR;
    tac->tac_globalvar = tac_gv;

    return tac;
}




TAC *tac_assign_new(TAC* destination, TAC *source, TAC_Assignment_Type assignop, int tacidx) {
    TACAssignment *tac_ass = malloc(sizeof(TACAssignment));
    tac_ass->assignop = assignop;
    tac_ass->source = source;
    tac_ass->destination = destination;

    TAC *tac = malloc(sizeof(TAC));
    tac->type = TAC_ASSIGNMENT;
    tac->tacidx = tacidx;
    tac->tac_assignemnt = tac_ass;

    return tac;
}



void tac_free(TAC* tac){

    assert(tac);
    
    if (tac->type == TAC_CONSTANT) {
        //printf("To be free: index: %d, CONSTANT=%d\n", i, tac->tac_constant->value);
        free(tac->tac_constant);
    }
    else if (tac->type == TAC_IDENTIFIER) {
        //printf("To be free: index: %d, IDENTIFIER=%s\n", i, tac->tac_identifier->name);
        free(tac->tac_identifier);
    }
    else if (tac->type == TAC_UNARY) {
        free(tac->tac_binary);
    }
    else if (tac->type == TAC_BINARY) {
        free(tac->tac_binary);
    }
    else if (tac->type == TAC_ASSIGNMENT) {
        free(tac->tac_assignemnt);
    }
    else if(tac->type == TAC_RELATIONAL) {
        free(tac->tac_relational);
    }
    else if(tac->type == TAC_LABEL) {
        free(tac->tac_label);
    }
    else if(tac->type == TAC_CONBRANCH) {
        free(tac->tac_condbranch);
    }
    else if(tac->type == TAC_JUMP) {
        free(tac->tac_jump);
    }
    else if(tac->type == TAC_CALL) {
        free(tac->tac_call);
    }
    else if(tac->type == TAC_STACK) {
        free(tac->tac_stack);
    }
    else if(tac->type == TAC_FPACCESS) {
        free(tac->tac_framepointer_access);
    }
    else if(tac->type == TAC_ENTRYPOINT) {
        
    }
    else if(tac->type == TAC_EXITPOINT) {
        
    }
    else if (tac->type == TAC_GLOBALVAR) {
        free(tac->tac_globalvar);
    }
    else if(tac->type == TAC_STACKPOINTER) {
        free(tac->tac_stack_pointer);
    }
    else {
        warnx("Unknown TAC! \n");
    }
    
    free(tac);
    
}


void free_tac_list(List* tac_list) {
    if (tac_list == NULL) {
        return;
    }

    for (int i = 0; i < list_length(tac_list); i++) {
        TAC* test = list_get(tac_list,i);
        tac_free(test);
    }

    list_free(tac_list);
}







void dump_const_ident_TAC_list(List* tac_list) {
    TAC* tac;

    for (int i = 0; i<list_length(tac_list); i++) {
        tac = (TAC*) list_get(tac_list, i);
        
        if(tac->type == TAC_CONSTANT) {
            printf("Index: %d, CONSTANT=%d\n", i, tac->tac_constant->value);
        }
        else if(tac->type == TAC_IDENTIFIER) {
            printf("Index: %d, IDENTIFIER=%s\n", i, tac->tac_identifier->name);
        }
    }
}
