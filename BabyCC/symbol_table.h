//
//  symbol_table.h
//  BabyCC
//
//  Created by Patricio Bulic on 06/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef symbol_table_h
#define symbol_table_h

#include <stdio.h>

typedef enum {
    ST_VARIABLE,
    ST_FUNCTION,
    ST_CONSTANT,
    ST_LABEL
} SymbolType;


typedef enum {
    ST_VOID,
    ST_CHAR,
    ST_SHORT,
    ST_INT,
    ST_LONG,
    ST_FLOAT,
    ST_DOUBLE,
    ST_SIGNED,
    ST_UNSIGNED,
    ST_BOOL,
    ST_COMPLEX,
    ST_IMAGINARY
} STTypeSpecifierType;

typedef enum {
    ST_GLOBAL,      // global declaration
    ST_LOCAL,       // local declaration
    ST_ARGUMENTS    // function argument declaration
} STVariableScopeType;

typedef struct VarSymbol{
    int stack_offset;  // offset from frame pointer
    int storage_reg;
    STTypeSpecifierType var_type;
    STVariableScopeType var_scope;
    /*TODO: add other attributes*/
} VarSymbol;

typedef struct LabelSymbol{
    void* address;
    /*TODO: add other attributes*/
} LabelSymbol;


typedef struct FunctionSymbol{
    void* address;
    STTypeSpecifierType func_type;
    STVariableScopeType var_scope;
    /*TODO: add other attributes*/
} FunctionSymbol;



typedef struct Symbol {
    SymbolType type;
    char* name;
    int scope;
    struct Symbol *next;
    
    union{
        VarSymbol *variable_symbol;
        LabelSymbol *label_symbol;
        FunctionSymbol *function_symbol;
    };
} Symbol;


/*TODO: SYMBOL TABLE SHOULD BE IMPLEMENTED AS A HASH TABLE*/
typedef struct SymbolTable {
    int length;
    Symbol *symbols;
} SymbolTable;



SymbolTable* symbol_table_new(void);
int symbol_table_length(SymbolTable* symtab);
void symbol_table_free(SymbolTable* symtab);
//void symbol_insert(SymbolTable* symtab, int scope, char* name, SymbolType type);
void symbol_insert(SymbolTable* symtab, Symbol *sym);
void symbol_remove(SymbolTable* symtab);
Symbol* symbol_get(SymbolTable* symtab, char* name);
bool symbol_lookup(SymbolTable* symtab, char* name);
bool symbol_lookup_in_scope(SymbolTable* symtab, int scope, char* name, STVariableScopeType var_scope);
void scope_close(SymbolTable* symtab, int scope);
int scope_length(SymbolTable* symtab, int scope);
Symbol* var_symbol_new(int scope, char* name, int stack_offset, int storage_reg, STTypeSpecifierType type, STVariableScopeType var_scope);
Symbol* func_symbol_new(int scope, char* name, STTypeSpecifierType type);
Symbol* label_symbol_new(int scope, char* name);

#endif /* symbol_table_h */
