//
//  tac.h
//  BabyCC
//
//  Created by Patricio Bulic on 07/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef tac_h
#define tac_h

#include <stdio.h>
#include "AST.h"
#include "list.h"



typedef enum {
    TAC_CONSTANT,
    TAC_IDENTIFIER,
    TAC_STACKPOINTER,
    TAC_STACK,
    TAC_ASSIGNMENT,
    TAC_UNARY,
    TAC_BINARY,
    TAC_RELATIONAL,
    TAC_LABEL,
    TAC_PARAMETERS,
    TAC_CONBRANCH,
    TAC_JUMP,
    TAC_FPACCESS,
    TAC_ENTRYPOINT,
    TAC_EXITPOINT,
    TAC_CALL,
    TAC_GLOBALVAR
} TACType;


typedef enum {
    TACOP_ADDITION,
    TACOP_SUBTRACTION,
    TACOP_MULTIPLICATION,
    TACOP_DIVISION,
    TACOP_MODULUS,
    TACOP_BIN_AND,
    TACOP_BIN_XOR,
    TACOP_BIN_OR,
    TACOP_LOG_AND,
    TACOP_LOG_OR,
    TACOP_SHL,
    TACOP_SHR,
    TACOP_PLUS,
    TACOP_MINUS,
    TACOP_ADDRESS,
    TACOP_DEREFERENCE,
    TACOP_BITWISENEG,
    TACOP_LOGNEG
} TAC_BINOP_Type;

typedef enum {
    TAC_EQUAL,
    TAC_NOT_EQUAL,
    TAC_LESS_THAN,
    TAC_LESS_THAN_OR_EQUAL,
    TAC_GREATER_THAN,
    TAC_GREATER_THAN_OR_EQUAL
} TAC_RELOP_Type;

typedef enum {
    TAC_SP_INC,
    TAC_SP_DEC
} TAC_STACKPOINTER_Type;

typedef enum {
    TAC_PUSH,
    TAC_POP
} TAC_STACK_Type;

typedef enum {
    TAC_BNE,
    TAC_BEQ
} TAC_CONDBRANCH_Type;

typedef enum {
    TAC_LOADFP,
    TAC_STOREFP
} TAC_FPACCESS_Type;   // Frame pointer acces type


typedef enum {
    TAC_ASSIGN,
    TAC_MUL_ASSIGN,
    TAC_DIV_ASSIGN,
    TAC_MOD_ASSIGN,
    TAC_ADD_ASSIGN,
    TAC_SUB_ASSIGN,
    TAC_LEFT_ASSIGN,
    TAC_RIGHT_ASSIGN,
    TAC_AND_ASSIGN,
    TAC_XOR_ASSIGN,
    TAC_OR_ASSIGN
} TAC_Assignment_Type;






struct TAC;
typedef struct TAC TAC;


typedef struct TACEntryPoint TACTACEntryPoint;  // empty as always produces the same code
typedef struct TACExitPoint TACTACExitPoint;    // empty as always produces the same code

typedef struct TACConstant {
    int value;
    //int storage_reg;  // index of the reg that holds the constant
} TACConstant;

typedef struct TACStackPointer {
    TAC_STACKPOINTER_Type type;
    int constant;
} TACStackPointer;

typedef struct TACFramePointerAccess {
    int stack_offset;
    TAC_FPACCESS_Type type;
    TAC* destination;
} TACFramePointerAccess;

typedef struct TACLabel {
    int labelidx;
    char* name;
} TACLabel;

typedef struct TACIdentifier {
    char* name;
    int storage_reg;  // index of the reg that holds the identifier
} TACIdentifier;

typedef struct TACCondBranch {
    TAC_CONDBRANCH_Type operation;
    TAC *source;
    TAC *branch_label;
} TACCondBranch;


typedef struct TACJump {
    TAC *branch_label;
} TACJump;

typedef struct TACCall {
    TAC *branch_label;
} TACCall;

typedef struct TACGlobalVar {
    TAC *source;
} TACGlobalVar;

typedef struct TACBinary {
    TAC_BINOP_Type operation;
    TAC *source1;
    TAC *source2;
    TAC *destination;
} TACBinary;

typedef struct TACRelational {
    TAC_RELOP_Type operation;
    TAC *source1;
    TAC *source2;
    TAC *destination;
} TACRelational;


typedef struct TACAssignment {
    TAC_Assignment_Type assignop;
    TAC *source;
    TAC *destination;
} TACAssignment;


typedef struct TACStack {
    TAC_STACK_Type type;
    TAC *source;
} TACStack;


struct TAC {
    TACType type;
    int tacidx;
    int labelidx;
    TAC *next;
    union {
        TACConstant *tac_constant;
        TACIdentifier *tac_identifier;
        TACLabel *tac_label;
        TACBinary *tac_binary;
        TACStack *tac_stack;
        TACRelational *tac_relational;
        TACAssignment *tac_assignemnt;
        TACCondBranch *tac_condbranch;
        TACCall *tac_call;
        TACJump *tac_jump;
        TACFramePointerAccess *tac_framepointer_access;
        TACStackPointer *tac_stack_pointer;
        TACGlobalVar *tac_globalvar;
    };
};


TAC *tac_constant_new(int value, int tacidx);
TAC *tac_label_new(int labelidx, char* name);
TAC *tac_identifier_new(char* name, int storage_reg);
TAC *tac_unary_new(TAC *source, TAC_BINOP_Type op, int tacidx);
TAC *tac_stack_new(TAC *source, TAC_STACK_Type op);
TAC *tac_binary_new(TAC *source1, TAC *source2, TAC_BINOP_Type op, int tacidx);
TAC *tac_relational_new(TAC *source1, TAC *source2, TAC_RELOP_Type op, int tacidx);
TAC *tac_assign_new(TAC* destination, TAC *source, TAC_Assignment_Type assignop, int tacidx);
TAC *tac_condbranch_new(TAC *source, TAC *label, TAC_CONDBRANCH_Type op, int tacidx);
TAC *tac_jump_new(TAC *label, int tacidx);
TAC *tac_call_new(TAC *label, int tacidx);
TAC *tac_framepointer_access_new(int stack_offset, TAC* destination, TAC_FPACCESS_Type type) ;
TAC *tac_entrypoint_new(void);
TAC *tac_exitpoint_new(void);
TAC *tac_stackpointer_new(int constant, TAC_STACKPOINTER_Type type);
TAC *tac_globalvar_new(TAC *source);

void tac_free(TAC* tac);
void free_tac_list(List* tac_list);



void dump_const_ident_TAC_list(List* tac_list);


//TAC* create_TAC(ASTnode *astnode, List *TAC_list);



#endif /* tac_h */
