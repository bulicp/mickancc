//
//  emmit.c
//  BabyCC
//
//  Created by Patricio Bulic on 04/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//


#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include "emmit.h"
#include "AST.h"
#include "list.h"
#include "symbol_table.h"
#include "tac.h"

extern int current_register;
extern int current_scope;
extern int current_scope_length;
extern int current_tacidx;
extern int current_labelidx;
extern int current_stack_offset;
extern int current_args_stack_offset;
extern SymbolTable* symtab;
extern List* tac_list_emmit;
extern List* tac_list_noemmit;
extern List* dataseg_tac_list;


static const int WORD_SIZE = 4;

void AST_parse(FILE* out, ASTnode *astnode, TAC **new_tac) {
    if (astnode->type == AST_CONSTANT) {
        fprintf(out, "%d", astnode->constant->value);
        *new_tac = tac_constant_new(astnode->constant->value, current_tacidx++);
        // this tac is non-emmitable.
        // append it to the no-emmit list to keep track of created tacs
        //list_append(tac_list_noemmit, *new_tac);
        list_append(tac_list_emmit, *new_tac);
        
    }
    else if (astnode->type == AST_IDENTIFIER) {
        fprintf(out, "%s", astnode->identifier->identifier_name);
        if (symbol_lookup(symtab, astnode->identifier->identifier_name) == false){
            printf("Undefined name(scope=%d): %s \n", current_scope, astnode->identifier->identifier_name);
            exit(EXIT_FAILURE);
        }
        
        // get the symbol from Symbol table
        
        Symbol* sym = symbol_get(symtab, astnode->identifier->identifier_name);
        if (sym->type == ST_FUNCTION) {
            //just create a new tac, do not add it to the list
            //*new_tac = tac_identifier_new(astnode->identifier->identifier_name);
            *new_tac = tac_identifier_new(sym->name, 0);
            list_append(tac_list_noemmit, *new_tac);
            //fprintf(out, "%s \n", astnode->identifier->identifier_name);
        }
        else if (sym->type == ST_VARIABLE) {
            //just create a new tac, do not add it to the emmit list
            //*new_tac = tac_identifier_new(astnode->identifier->identifier_name);
            *new_tac = tac_identifier_new(sym->name, sym->variable_symbol->storage_reg);
            list_append(tac_list_noemmit, *new_tac);
            //printf("Symbol: %s, offset=%d, t%d \n", sym->name, sym->variable_symbol->stack_offset, sym->variable_symbol->storage_reg);
        }
        
        
    }
    else if (astnode->type == AST_TYPE_SPECIFIER) {
        ASTTypeSpecifier *ast = astnode->type_specifier;
        if (ast->type == TS_INT) {
            fprintf(out, "INT ");
        }
        else if (ast->type == TS_BOOL) {
            fprintf(out, "BOOL ");
        }
        else if (ast->type == TS_CHAR) {
            fprintf(out, "CHAR ");
        }
        else if (ast->type == TS_SHORT) {
            fprintf(out, "HALF ");
        }
        else {
            /*TODO: add other types*/
            fprintf(out, "SOMETYPE ");
        }
        
    }
    
    else if (astnode->type == AST_UNARY_OP) {
        ASTUnaryExpression *ast = astnode->unary_expression;
        TAC_BINOP_Type operation = TACOP_PLUS;
        
        if (ast->type == UE_PLUS) {
            fprintf(out, "+(");
            operation = TACOP_PLUS;
        }
        else if (ast->type == UE_MINUS) {
            fprintf(out, "-(");
            operation = TACOP_MINUS;
        }
        else if (ast->type == UE_LOGNEG) {
            fprintf(out, "!(");
            operation = TACOP_LOGNEG;
        }
        else if (ast->type == UE_ADDRESS) {
            fprintf(out, "&(");
            operation = TACOP_ADDRESS;
        }
        else if (ast->type == UE_BITWISENEG) {
            fprintf(out, "~(");
            operation = TACOP_BITWISENEG;
        }
        else if (ast->type == UE_DEREFERENCE) {
            fprintf(out, "*(");
            operation = TACOP_DEREFERENCE;
        }
        
        TAC* right;
        AST_parse(out, ast->right, &right);
        fprintf(out, ")");
        
       
        TAC* tacunary = tac_unary_new(right,
                                    operation,
                                    current_tacidx++);
        
        list_append(tac_list_emmit, tacunary);
        
        *new_tac = tacunary;
    }
    
    else if (astnode->type == AST_BINARY_OP) {
        ASTBinaryExpression *ast = astnode->binary_expression;
        TAC_BINOP_Type operation = TACOP_MULTIPLICATION;
        
        if (ast->type == BE_MULTIPLICATION) {
            fprintf(out, "MUL(");
            operation = TACOP_MULTIPLICATION;
        }
        else if (ast->type == BE_ADDITION) {
            fprintf(out, "ADD(");
            operation = TACOP_ADDITION;
        }
        else if (ast->type == BE_SUBTRACTION) {
            fprintf(out, "SUB(");
            operation = TACOP_SUBTRACTION;
        }
        else if (ast->type == BE_DIVISION) {
            fprintf(out, "DIV(");
            operation = TACOP_DIVISION;
        }
        else if (ast->type == BE_MODULUS) {
            fprintf(out, "MOD(");
            operation = TACOP_MODULUS;
        }
        else if (ast->type == BE_BIN_OR) {
            fprintf(out, "BIN_OR(");
            operation = TACOP_BIN_OR;
        }
        else if (ast->type == BE_BIN_AND) {
            fprintf(out, "BIN_AND(");
            operation = TACOP_BIN_AND;
        }
        else if (ast->type == BE_BIN_XOR) {
            fprintf(out, "BIN_XOR(");
            operation = TACOP_BIN_XOR;
        }
        else if (ast->type == BE_LOG_OR) {
            fprintf(out, "LOG_OR(");
            operation = TACOP_LOG_OR;
        }
        else if (ast->type == BE_LOG_AND) {
            fprintf(out, "LOG_AND(");
            operation = TACOP_LOG_AND;
        }
        
        
        TAC* left;
        TAC* right;
        
        AST_parse(out, ast->left, &left);
        fprintf(out, ", ");
        AST_parse(out, ast->right, &right);
        fprintf(out, ")");
        
        
        
        TAC* tacbin = tac_binary_new(left,
                                     right,
                                     operation,
                                     current_tacidx++);
        
        // appent the tac to the emmit list
        list_append(tac_list_emmit, tacbin);
        
        *new_tac = tacbin;
        
    }
    
    else if (astnode->type == AST_SHIFT_OP) {
        ASTShiftExpression *ast = astnode->shift_expression;
        TAC_BINOP_Type operation = TACOP_SHL;
        
        if (ast->type == SE_LEFT) {
            fprintf(out, "SHL(");
            operation = TACOP_SHL;
        }
        else if (ast->type == SE_RIGHT) {
            fprintf(out, "SHR(");
            operation = TACOP_SHR;
        }
        
        
        TAC* left;
        TAC* right;
        
        AST_parse(out, ast->left, &left);
        fprintf(out, ", ");
        AST_parse(out, ast->right, &right);
        fprintf(out, ")");
        
        
        
        TAC* tacbin = tac_binary_new(left,
                                     right,
                                     operation,
                                     current_tacidx++);
        
        list_append(tac_list_emmit, tacbin);
        
        *new_tac = tacbin;
        
    }
    
    else if (astnode->type == AST_EQAULITY_OP) {
        ASTEqualityExpression *ast = astnode->equality_expression;
        TAC_RELOP_Type relop = TAC_EQUAL;
        
        if (ast->type == EE_EQUAL) {
            fprintf(out, "EQ?(");
            relop = TAC_EQUAL;
        }
        else if (ast->type == EE_NOT_EQUAL) {
            fprintf(out, "NEQ?(");
            relop = TAC_NOT_EQUAL
            ;
        }
        
        TAC* left;
        TAC* right;
        
        AST_parse(out, ast->left, &left);
        fprintf(out, ", ");
        AST_parse(out, ast->right, &right);
        fprintf(out, ")");
        
        TAC* tacrel = tac_relational_new(left,
                                         right,
                                         relop,
                                         current_tacidx++);
        list_append(tac_list_emmit, tacrel);
        *new_tac = tacrel;
        
    }
    
    else if (astnode->type == AST_RELATIONAL_OP) {
        ASTRelationalExpression *ast = astnode->relational_expression;
        TAC_RELOP_Type relop = TAC_LESS_THAN;
        
        if (ast->type == RE_LESS_THAN) {
            fprintf(out, "LT(");
            relop = TAC_LESS_THAN;
        }
        else if (ast->type == RE_LESS_THAN_OR_EQUAL) {
            fprintf(out, "LTE(");
            relop = TAC_LESS_THAN_OR_EQUAL;
        }
        else if (ast->type == RE_GREATER_THAN) {
            fprintf(out, "GT(");
            relop = TAC_GREATER_THAN;
        }
        else if (ast->type == RE_GREATER_THAN_OR_EQUAL) {
            fprintf(out, "GTE(");
            relop = TAC_GREATER_THAN_OR_EQUAL;
        }
        
        TAC* left;
        TAC* right;
        
        AST_parse(out, ast->left, &left);
        fprintf(out, ", ");
        AST_parse(out, ast->right, &right);
        fprintf(out, ")");
        
        
        TAC* tacrel = tac_relational_new(left,
                                         right,
                                         relop,
                                         current_tacidx++);
        list_append(tac_list_emmit, tacrel);
        *new_tac = tacrel;
        
    }
     
    
    else if (astnode->type == AST_ASSIGNMENT_EX) {
        ASTAssignmentExpression *ast = astnode->assignment_expression;
        TAC_BINOP_Type operation = TACOP_ADDITION;
        TAC* left = NULL;
        TAC* right = NULL;
        
        
        AST_parse(out, ast->left, &left);
        //fprintf(out, " = ");
        
        if (ast->type == AE_ASSIGN) {
            fprintf(out, " = ");
            AST_parse(out, ast->right, &right);
            
            TAC* tacass = tac_assign_new(left,
                                         right,
                                         TAC_ASSIGN,
                                         current_tacidx++);
            
            list_append(tac_list_emmit, tacass);
            
            *new_tac = tacass;
        }
        else {
            if (ast->type == AE_OR_ASSIGN) {
                //AST_parse(out, ast->left, &left);
                fprintf(out, " |= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_BIN_OR;
            }
            
            else if (ast->type == AE_ADD_ASSIGN) {
                //AST_parse(out, ast->left, &left);
                fprintf(out, " += ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_ADDITION;
            }
            else if (ast->type == AE_AND_ASSIGN) {
                //AST_parse(out, ast->left, &left);
                fprintf(out, " &= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_BIN_AND;
            }
            else if (ast->type == AE_DIV_ASSIGN) {
                //AST_parse(out, ast->left, &left);
                fprintf(out, " /= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_DIVISION;
            }
            else if (ast->type == AE_MOD_ASSIGN) {
                //AST_parse(out, ast->left, &left);
                fprintf(out, " %%= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_MODULUS;
            }
            else if (ast->type == AE_SUB_ASSIGN) {
                fprintf(out, " -= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_SUBTRACTION;
            }
            else if (ast->type == AE_XOR_ASSIGN) {
                fprintf(out, " ^= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_BIN_XOR;
            }
            else if (ast->type == AE_LEFT_ASSIGN) {
                fprintf(out, " <<= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_SHL;
            }
            else if (ast->type == AE_RIGHT_ASSIGN) {
                fprintf(out, " >>= ");
                AST_parse(out, ast->right, &right);
                operation = TACOP_SHR;
            }
            else {
                warnx("DUMP ERROR: Unknown TAC!");
            }
            
            TAC* tacbin = tac_binary_new(left,
                                         right,
                                         operation,
                                         current_tacidx++);
            
            TAC* tacass = tac_assign_new(left,
                                         tacbin,
                                         TAC_ASSIGN,
                                         current_tacidx++);
            
            list_append(tac_list_emmit, tacbin);
            list_append(tac_list_emmit, tacass);
            
            *new_tac = tacass;
            
        }
        
        //fprintf(out, ")");
    }
    
    
    else if (astnode->type == AST_INIT_DECLARATOR) {
        ASTInitDeclarator *ast = astnode->init_declarator;
        
        TAC* left;
        TAC* right;
        if (ast->type == ID_NOINIT) {
            AST_parse(out, ast->declarator, &left);
             *new_tac = left;
        }
        else if (ast->type == ID_INIT) {
            AST_parse(out, ast->declarator, &left);
            fprintf(out, " = ");
            AST_parse(out, ast->initializer, &right);
        }
        
    }
    
    
    else if (astnode->type == AST_JUMP_STATEMENT) {
        ASTJumpStatement *ast = astnode->jump_statement;
        TAC* ret;
        if (ast->type == JS_RETURN_EXP) {
            fprintf(out, "RET(");
            AST_parse(out, ast->expression, &ret);
            fprintf(out, ")");
        }
        else if (ast->type == JS_GOTO) {
            
        }
        else if (ast->type == JS_BREAK) {
            
        }
        else if (ast->type == JS_CONTINUE) {
            
        }
        fprintf(out, "\n");
    }
    
    else if (astnode->type == AST_ITERATION_STATEMENT) {
        ASTIterationStatement *ast = astnode->iteration_statement;
        TAC* left;
        TAC* right;
        
        
        if (ast->type == IS_WHILE) {
            fprintf(out, "WHILE(");
            
            // create the first label TAC and append it to the list
            TAC* label1 = tac_label_new(current_labelidx++, NULL);
            //current_labelidx ++;
            list_append(tac_list_emmit, label1);
            
            // create the second label TAC but append it to the list at the end of WHILE block
            TAC* label2 = tac_label_new(current_labelidx++, NULL);
            //current_labelidx ++;
            
            AST_parse(out, ast->condition, &left); //this will create relational TAC and append it to the list
            
            // create conditional branch TAC to exit the loop:
            TAC* cb = tac_condbranch_new(left, label2, TAC_BEQ, current_tacidx);
            list_append(tac_list_emmit, cb);
            
            
            fprintf(out, ") { \n");
            AST_parse(out, ast->statement, &right);
            fprintf(out, "}");
            
            // create jump TAC to loop:
            TAC* c = tac_jump_new(label1, current_tacidx);
            list_append(tac_list_emmit, c);
            // now append the exit label to the TAC list:
            list_append(tac_list_emmit, label2);
            
        }
        
    
        
        else if (ast->type == IS_DO) {
            // TODO:
        }
        else if (ast->type == IS_FOR) {
            // TODO:
            //fprintf(out, "FOR(");
            
            // create the first label TAC
            TAC* label1 = tac_label_new(current_labelidx++, NULL);
            
            // create the second label TAC but append it to the list at the end of FOR block
            TAC* label2 = tac_label_new(current_labelidx++, NULL);
            
            
            // INITIALIZATION:
            TAC *forinit;
            AST_parse(out, ast->forinit, &forinit); //this will create assignemnt TAC and append it to the list
            
            // now append the first label
            list_append(tac_list_emmit, label1);
            
            // CONDITION:
            TAC* cond;
            AST_parse(out, ast->condition, &cond); //this will create relational TAC and append it to the list
            
            // create conditional branch TAC to exit the loop:s
            TAC* cb = tac_condbranch_new(cond, label2, TAC_BEQ, current_tacidx);
            list_append(tac_list_emmit, cb);
            
            TAC* forupdate;
            AST_parse(out, ast->forupdate, &forupdate); //this will create assignemnt TAC and append it to the list
            
            TAC* statement;
            fprintf(out, ") { \n");
            AST_parse(out, ast->statement, &statement);
            fprintf(out, "}");
            
            
            // create jump TAC to loop:
            TAC* c = tac_jump_new(label1, current_tacidx);
            list_append(tac_list_emmit, c);
            // now append the exit label to the TAC list:
            list_append(tac_list_emmit, label2);
            
        }
        fprintf(out, "\n");
    }
    
    
    else if (astnode->type == AST_SELECTION_STATEMENT) {
        ASTSelectionStatement *ast = astnode->selection_statement;
        TAC* cond;
        TAC* true_tac;
        TAC* false_tac;
        
        if (ast->type == SS_IF) {
            fprintf(out, "IF(");
            
            // create the "false" label TAC but append it to the list at the end of IF block
            TAC* label_false = tac_label_new(current_labelidx++, NULL);
            
            AST_parse(out, ast->condition, &cond); //this will create relational TAC and append it to the list
            
            // create conditional branch TAC to jump to FALSE:
            TAC* cb = tac_condbranch_new(cond, label_false, TAC_BEQ, current_tacidx);
            list_append(tac_list_emmit, cb);
            
            fprintf(out, ") { \n");
            AST_parse(out, ast->true_statement, &true_tac);
            fprintf(out, "}");
            
            // now append the exit label to the TAC list:
            list_append(tac_list_emmit, label_false);
        }
        
        else if (ast->type == SS_IFELSE) {
            fprintf(out, "IF(");
            
            
            // create the "true" label TAC but append it to the list at the end of TRUE block
            TAC* label_else = tac_label_new(current_labelidx++, NULL);
            // create the "exit" label TAC but append it to the list at the end of IF-ELSE block
            TAC* label_end = tac_label_new(current_labelidx++, NULL);
            
            AST_parse(out, ast->condition, &cond); //this will create relational TAC and append it to the list
            
            // create conditional branch TAC to jump to FALSE:
            TAC* cb = tac_condbranch_new(cond, label_else, TAC_BEQ, current_tacidx);
            list_append(tac_list_emmit, cb);
            
            // parse if block;
            fprintf(out, ") { \n");
            AST_parse(out, ast->true_statement, &true_tac);
            fprintf(out, "}");
            
            // create jump TAC to exit IF block:
            TAC* c = tac_jump_new(label_end, current_tacidx);
            list_append(tac_list_emmit, c);
            
            list_append(tac_list_emmit, label_else);
            
            // parse else block;
            fprintf(out, "ELSE { \n");
            AST_parse(out, ast->false_statement, &false_tac);
            fprintf(out, "}");
            
            list_append(tac_list_emmit, label_end);
            
        }
        else if (ast->type == SS_SWITCH) {
            // TODO:
        }
        fprintf(out, "\n");
    }
    
    
    
    else if (astnode->type == AST_DECLARATION_STATEMENT) {
        ASTDeclarationStatement *ast = astnode->declaration_statement;
        STVariableScopeType var_scope;
        
        if (ast->type == DS_LOCAL) {
            fprintf(out, "LOCAL ");
            var_scope = ST_LOCAL;
        }
        else if (ast->type == DS_GLOBAL) {
            fprintf(out, "GLOBAL ");
            var_scope = ST_GLOBAL;
        }
        if (ast->type == DS_ARGUMENTS) {
            fprintf(out, "ARGUMENT ");
            var_scope = ST_GLOBAL;
        }
        
        
        char *varname = ast->declarator_list->init_declarator->declarator->identifier->identifier_name;
        STTypeSpecifierType vartype = ST_INT; // TODO: get type from AST!!!!!
        
        if (ast->type == DS_LOCAL){
            if (symbol_lookup_in_scope(symtab, current_scope, varname, var_scope) == false){
                // add new variable to current scope:
                Symbol* sym = var_symbol_new(current_scope, varname, current_stack_offset, current_tacidx++, vartype, var_scope);
                current_stack_offset -= WORD_SIZE;
                //symbol_insert(symtab, current_scope, varname, ST_VARIABLE);
                symbol_insert(symtab, sym);
                //printf("NEW VAR (scope %d): %s, offset = %d, t%d \n", current_scope, varname, sym->variable_symbol->stack_offset, sym->variable_symbol->storage_reg);
            }
        }
        else if (ast->type == DS_GLOBAL){
            
            //if (symbol_lookup_in_scope(symtab, current_scope, varname) == false)
            if (symbol_lookup_in_scope(symtab, 0, varname, var_scope) == false){
                // add new variable to scope 0:
                Symbol* sym = var_symbol_new(0, varname, 0, current_tacidx++, vartype, var_scope);
                symbol_insert(symtab, sym);

            }
        }
        
        
        TAC *left;
        TAC *right;
        
        AST_parse(out, ast->specifier, &left);
        fprintf(out, " ");
        
        AST_parse(out, ast->declarator_list, &right);
        if(ast->type == DS_LOCAL){
            fprintf(out, ", stack address: fp+%d", current_stack_offset+WORD_SIZE);
            fprintf(out, "\n");
        }
        else if(ast->type == DS_GLOBAL) {
            //fprintf(out, "", current_stack_offset+WORD_SIZE);
            fprintf(out, "\n");
        }
        
        if(ast->type == DS_LOCAL){
            // local's declaration :
            TAC* sp = tac_stackpointer_new(WORD_SIZE, TAC_SP_DEC);
            list_append(tac_list_emmit, sp);
            
            // loacal's access (load local var into a register)
            TAC* fpa = tac_framepointer_access_new(symtab->symbols->variable_symbol->stack_offset, right, TAC_LOADFP);
            //current_tacidx++;
            list_append(tac_list_emmit, fpa);
        }
        else if(ast->type == DS_GLOBAL){
            // PUSH the TAC for global declaration at the begining of the TAQC list:
            TAC* gv = tac_globalvar_new(right);
            // append tac to the data segment tac list:
            list_append(dataseg_tac_list, gv);
            // and push tac to the beginning of the tac list:
            list_push(tac_list_emmit, gv);
        }

    }
    
    
    
    else if (astnode->type == AST_PARAMETER_DECLARATION) {
        ASTParameterDeclaration *ast = astnode->parameter_declaration;
        STVariableScopeType var_scope;
        
        if (ast->type == DS_LOCAL) {
            fprintf(out, "LOCAL ");
            var_scope = ST_LOCAL;
        }
        else if (ast->type == DS_GLOBAL) {
            fprintf(out, "GLOBAL ");
            var_scope = ST_GLOBAL;
        }
        if (ast->type == DS_ARGUMENTS) {
            fprintf(out, "ARGUMENT ");
            var_scope = ST_ARGUMENTS;
        }
        
        
        char *varname = ast->declarator->identifier->identifier_name;
        STTypeSpecifierType vartype = ST_INT; // TODO: get type from AST!!!!!
        
        
        if (symbol_lookup_in_scope(symtab, current_scope, varname, var_scope) == false){
            // add new variable:
            Symbol* sym = var_symbol_new(current_scope, varname, current_args_stack_offset, current_tacidx++, vartype, var_scope);
            current_args_stack_offset += WORD_SIZE;
            symbol_insert(symtab, sym);
        }
        
         
        
        TAC *left;
        TAC *right;
        
        AST_parse(out, ast->specifier, &left);
        fprintf(out, " ");
        
        AST_parse(out, ast->declarator, &right);
        fprintf(out, ", stack address: fp+%d", current_args_stack_offset-WORD_SIZE);
        fprintf(out, "\n");
        
        
        // argument access (load argument var into a register)
        TAC* fpa = tac_framepointer_access_new(symtab->symbols->variable_symbol->stack_offset, right, TAC_LOADFP);
        //current_tacidx++;
        list_append(tac_list_emmit, fpa);
        
        
    }
    
    
    else if (astnode->type == AST_PARAMETER_LIST) {
        ASTParametertList *ast = astnode->parameter_list;
        TAC *dummy;
        
        current_args_stack_offset = 12; //reset stack offset for arguments
        
        
        /*
         * We should parse all declarations in the parameter list
         */
        List *param_declarations = ast->parameter_declarations;
        
        for (int i = list_length(param_declarations)-1; i >= 0; i--) {
            AST_parse(out, list_get(param_declarations, i), &dummy);
        }
    }
    
    else if (astnode->type == AST_ARGUMENT_LIST) {
        ASTArgumentList *ast = astnode->argument_list;
        TAC *arg;
        
        /*
         * We should parse all declarations in the parameter list
         */
        List *arguments = ast->arguments;
        
        for (int i = list_length(arguments)-1; i >= 0; i--) {
            AST_parse(out, list_get(arguments, i), &arg);
            if (i>0) {   // do not print comma after the last argument
                fprintf(out, ", ");
            }
            // local's declaration :
            TAC* stack = tac_stack_new(arg, TAC_PUSH);
            list_append(tac_list_emmit, stack);
        }
    }
    
    
    
    else if (astnode->type == AST_DECLARATION_LIST) {
        ASTDeclarationList *ast = astnode->declaration_list;
        TAC *dummy;
        // TODO:
        printf("declaration list!\n");
        
    }
    
    else if (astnode->type == AST_EXPRESSION_STATEMENT) {
        ASTExpressionStatement *ast = astnode->expression_statement;
        TAC *dummy;
        if (ast->type == ES_EXPRESSION) {
            AST_parse(out, ast->expression, &dummy);
            // just new line to designate the statement
            fprintf(out, ";\n");
            *new_tac = dummy;
        }
    }
    
    else if (astnode->type == AST_BLOCK) {
        ASTBlock *ast = astnode->block;
        TAC *dummy;
        /*
         * We should dump all statements in a Block's list
         */
        List *statements = ast->statements;
        
        for (int i = list_length(statements)-1; i >= 0; i--) {
            //ASTnode *statement = list_get(statements, i);
            AST_parse(out, list_get(statements, i), &dummy);
        }
        
    }
    
    else if (astnode->type == AST_COMPOUND_STATEMENT) {
        ASTCompoundStatement *ast = astnode->compound_statement;
        TAC *dummy;
        // set a new scope::
        current_scope ++;
        current_scope_length = 0;
        
        fprintf(out, "<<< \n");
        AST_parse(out, ast->block, &dummy);
        fprintf(out, ">>> \n");
        
        current_scope_length = scope_length(symtab, current_scope);
        //printf("scope length = %d\n",current_scope_length);
        scope_close(symtab, current_scope);
        current_scope--;
    }
    
    
    
    else if (astnode->type == AST_FUNCTION_DECLARATOR) {
        ASTFunctionDeclarator *ast = astnode->function_declarator;
        //AST_dump(out, ast->declaration_specifiers);
        TAC *left;
        TAC *right;
        fprintf(out, "FUNCTION DECLARATOR: \n");
        //fprintf(out, "\n");
        AST_parse(out, ast->func_identifier, &left);
        fprintf(out, ": \n");
        if (ast->type == FD_ARGS) {
            AST_parse(out, ast->parameter_type_list, &right);
        }
    }
    
    
    else if (astnode->type == AST_FUNCTION_DEFINITION) {
        ASTFunctionDefinition *ast = astnode->function_definition;
        
        current_stack_offset = 0; // reset stack offset for locals
        
        // Function names are placed into the global scope 0 in symbol table
        char* funcname = ast->declarator->function_declarator->func_identifier->identifier->identifier_name;
        STTypeSpecifierType functype = ST_INT;
        
        if (symbol_lookup_in_scope(symtab, current_scope, funcname, ST_GLOBAL) == false){
            // add new function name:
           
            Symbol* sym = func_symbol_new(current_scope, funcname, functype);
            symbol_insert(symtab, sym);
            
            //symbol_insert(symtab, current_scope, funcname, ST_FUNCTION);
            printf("NEW FUNCT(scope=%d): %s\n", current_scope, funcname);
        }
        
        TAC *dummy;
        
        // create the first label TAC and append it to the list
        TAC* flabel = tac_label_new(current_labelidx++, funcname);
        //current_labelidx ++;
        list_append(tac_list_emmit, flabel);
        
        TAC* tac_entryp = tac_entrypoint_new();
        list_append(tac_list_emmit, tac_entryp);
        
        
        //AST_parse(out, ast->declaration_specifiers);
        fprintf(out, "\n");
        AST_parse(out, ast->declarator, &dummy);
        
        //help(ast->compound_statement);
        
        //fprintf(out, ": \n");
        AST_parse(out, ast->compound_statement, &dummy);
        
        
        // pop locals from stack:
        TAC* sp = tac_stackpointer_new(WORD_SIZE*current_scope_length, TAC_SP_INC);
        list_append(tac_list_emmit, sp);
        
        TAC* tac_exitp = tac_exitpoint_new();
        list_append(tac_list_emmit, tac_exitp);
        
    }
    
    
    else if (astnode->type == AST_FUNCTION_CALL) {
        ASTFunctionCall *ast = astnode->function_call;
        
        TAC *left;
        TAC *right;
    
        fprintf(out, "CALL (");
        
        AST_parse(out, ast->name, &left);
        if(ast->type == FD_NOARGS) {
            
        }
        else if(ast->type == FD_ARGS) {
            fprintf(out, "(");
            AST_parse(out, ast->argument_list, &right);
            fprintf(out, ")");
        }
        
        TAC* tc = tac_call_new(left, current_tacidx);
        list_append(tac_list_emmit, tc);
        
        
        // pop arguments from stack:
        int nargs = list_length(ast->argument_list->argument_list->arguments);
        TAC* sp = tac_stackpointer_new(WORD_SIZE*nargs, TAC_SP_INC);
        list_append(tac_list_emmit, sp);
    }
    
    
    else if (astnode->type == AST_TRANSLATIONAL_UNIT) {
        ASTTranslationalUnit *ast = astnode->translational_unit;
        TAC *dummy;
        /*
         * We should dump all declaration statements and function definitions in a TU list
         */
        List *ext_declarations = ast->ext_declarations;
        
        // First, parse global eclarations:
        for (int i = 0; i < list_length(ext_declarations); i++) {
            ASTnode *statement = list_get(ext_declarations, i);
            if (statement->type == AST_DECLARATION_STATEMENT) {
                AST_parse(out, list_get(ext_declarations, i), &dummy);
            }
        }
        
        for (int i = 0;  i < list_length(ext_declarations); i++) {
            ASTnode *statement = list_get(ext_declarations, i);
            if (statement->type == AST_FUNCTION_DEFINITION) {
                AST_parse(out, list_get(ext_declarations, i), &dummy);
            }
        }
    }
    
    else {
        warnx("DUMP ERROR: Unknown AST node %s", AST_type_name(astnode));
    }
    
}





void dump_dataseg_TAC_list(FILE* out, List* tac_list) {
    TAC* tac;

    // emmit header:
    fprintf(out, "   .data \n");
    fprintf(out, "   .org 0x400 \n");
    
    for (int i = 0; i<list_length(tac_list); i++) {
        tac = (TAC*) list_get(tac_list, i);
    
        if (tac->type == TAC_GLOBALVAR) {
            fprintf(out, "%s:", tac->tac_globalvar->source->tac_identifier->name);
            fprintf(out, " .space 4\n");
        }
        else {
            
        }
    }
    fprintf(out, "\n");
}





void dump_TAC_list(FILE* out, List* tac_list_emmit) {
    TAC* tac;
    
    // emmit header:
    fprintf(out, "   .code \n");
    fprintf(out, "   .org 0x0 \n");
    fprintf(out, "   addui sp, r0, #0x4fc \n\n");
    
    
    
    for (int i = 0; i<list_length(tac_list_emmit); i++) {
        tac = (TAC*) list_get(tac_list_emmit, i);
        
        if (tac->type == TAC_CONSTANT) {
            fprintf(out, "   addui t%d, r0, #%d \n", tac->tacidx, tac->tac_constant->value);
        }
        
        if(tac->type == TAC_BINARY) {
            
            
            if (tac->tac_binary->operation == TACOP_ADDITION){
                fprintf(out, "   add ");
            }
            else if (tac->tac_binary->operation == TACOP_SUBTRACTION){
                fprintf(out, "   sub ");
            }
            else if (tac->tac_binary->operation == TACOP_MULTIPLICATION){
                fprintf(out, "   mul ");
            }
            else if (tac->tac_binary->operation == TACOP_DIVISION){
                fprintf(out, "   div ");
            }
            else if (tac->tac_binary->operation == TACOP_MODULUS){
                fprintf(out, "   mod ");
            }
            else if (tac->tac_binary->operation == TACOP_BIN_OR){
                fprintf(out, "   or ");
            }
            else if (tac->tac_binary->operation == TACOP_BIN_AND){
                fprintf(out, "   and ");
            }
            else if (tac->tac_binary->operation == TACOP_BIN_XOR){
                fprintf(out, "   xor ");
            }
            else if (tac->tac_binary->operation == TACOP_LOG_AND){
                fprintf(out, "   land ");
            }
            else if (tac->tac_binary->operation == TACOP_LOG_OR){
                fprintf(out, "   lor ");
            }
            else if (tac->tac_binary->operation == TACOP_SHR){
                fprintf(out, "   shr ");
            }
            else if (tac->tac_binary->operation == TACOP_SHL){
                fprintf(out, "   shl ");
            }
            
            
            fprintf(out, "t%d, ", tac->tacidx);
            
            if(tac->tac_binary->source1->type == TAC_CONSTANT) {
                fprintf(out, "t%d",  tac->tac_binary->source1->tacidx );
            }
            else if (tac->tac_binary->source1->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name);
                fprintf(out, "t%d",  tac->tac_binary->source1->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d", tac->tac_binary->source1->tacidx);
            }
            
            fprintf(out, ", ");
            
            if(tac->tac_binary->source2->type == TAC_CONSTANT) {
                fprintf(out, "t%d",  tac->tac_binary->source2->tacidx );
            }
            else if (tac->tac_binary->source2->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_binary->source2->tac_identifier->name);
                fprintf(out, "t%d",  tac->tac_binary->source2->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d", tac->tac_binary->source2->tacidx);
            }
            
            
            fprintf(out, "\n");
            
        }
        
        
        else if(tac->type == TAC_UNARY) {
            
            
            if (tac->tac_binary->operation == TACOP_PLUS){
                fprintf(out, "   add ");
                fprintf(out, "t%d, ", tac->tacidx);
                fprintf(out, "r0, ");
                
                if(tac->tac_binary->source1->type == TAC_CONSTANT) {
                    fprintf(out, "t%d",  tac->tac_binary->source1->tacidx );
                }
                else if (tac->tac_binary->source1->type == TAC_IDENTIFIER) {
                    //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name);
                    fprintf(out, "t%d",  tac->tac_binary->source1->tac_identifier->storage_reg);
                }
                else {
                    fprintf(out, "t%d", tac->tac_binary->source1->tacidx);
                }
                
                
            }
            else if (tac->tac_binary->operation == TACOP_MINUS){
                fprintf(out, "   sub ");
                fprintf(out, "t%d, ", tac->tacidx);
                fprintf(out, "r0, ");
                
                if(tac->tac_binary->source1->type == TAC_CONSTANT) {
                    fprintf(out, "t%d",  tac->tac_binary->source1->tacidx );
                }
                else if (tac->tac_binary->source1->type == TAC_IDENTIFIER) {
                    //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name);
                    fprintf(out, "t%d",  tac->tac_binary->source1->tac_identifier->storage_reg);
                }
                else {
                    fprintf(out, "t%d", tac->tac_binary->source1->tacidx);
                }
            }
            
            else if (tac->tac_binary->operation == TACOP_ADDRESS){
                fprintf(out, "   addr TODO");
            }
            else if (tac->tac_binary->operation == TACOP_DEREFERENCE){
                fprintf(out, "   deref TODO");
            }
            else if (tac->tac_binary->operation == TACOP_BITWISENEG){
                fprintf(out, "   not ");
                fprintf(out, "t%d, ", tac->tacidx);
                
                
                
                if(tac->tac_binary->source1->type == TAC_CONSTANT) {
                    fprintf(out, "t%d,",  tac->tac_binary->source1->tacidx );
                }
                else if (tac->tac_binary->source1->type == TAC_IDENTIFIER) {
                    //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name);
                    fprintf(out, "t%d,",  tac->tac_binary->source1->tac_identifier->storage_reg);
                }
                else {
                    fprintf(out, "t%d,", tac->tac_binary->source1->tacidx);
                }
                
                fprintf(out, " r0");
                
            }
            else if (tac->tac_binary->operation == TACOP_LOGNEG){
                fprintf(out, "   logneg TODO");
            }
            
            
            
            
            
            fprintf(out, "\n");
            
        }
        
        
        else if(tac->type == TAC_ASSIGNMENT) {
            if(tac->tac_assignemnt->destination->type == TAC_IDENTIFIER) {
                //fprintf(out, "   mov %s",  tac->tac_assignemnt->destination->tac_identifier->name );
                fprintf(out, "   addui t%d",  tac->tac_assignemnt->destination->tac_identifier->storage_reg);
            }
            
            fprintf(out, ", ");
            
            if(tac->tac_assignemnt->source->type == TAC_CONSTANT) {
                fprintf(out, "t%d, #0",  tac->tac_assignemnt->source->tacidx );
            }
            else if (tac->tac_assignemnt->source->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_assignemnt->source->tac_identifier->name );
                fprintf(out, "t%d, #0",  tac->tac_assignemnt->source->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d, #0", tac->tac_assignemnt->source->tacidx);
            }
            
            
            fprintf(out, "\n");
            
        }
        
        else if(tac->type == TAC_RELATIONAL) {
            
            
            if (tac->tac_relational->operation == TAC_LESS_THAN){
                fprintf(out, "   slt ");
            }
            else if (tac->tac_relational->operation == TAC_LESS_THAN_OR_EQUAL){
                fprintf(out, "   slte ");
            }
            else if (tac->tac_relational->operation == TAC_GREATER_THAN){
                fprintf(out, "   sgt ");
            }
            else if (tac->tac_relational->operation == TAC_GREATER_THAN_OR_EQUAL){
                fprintf(out, "   sgte ");
            }
            else if (tac->tac_relational->operation == TAC_EQUAL){
                fprintf(out, "   seq ");
            }
            else if (tac->tac_relational->operation == TAC_NOT_EQUAL){
                fprintf(out, "   sneq ");
            }
            
            fprintf(out, "t%d, ", tac->tacidx);
            
            if(tac->tac_relational->source1->type == TAC_CONSTANT) {
                fprintf(out, "t%d",  tac->tac_relational->source1->tacidx );
            }
            else if (tac->tac_relational->source1->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name );
                fprintf(out, "t%d",  tac->tac_relational->source1->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d", tac->tac_relational->source1->tacidx);
            }
            
            fprintf(out, ", ");
            
            if(tac->tac_relational->source2->type == TAC_CONSTANT) {
                fprintf(out, "t%d",  tac->tac_relational->source2->tacidx );
            }
            else if (tac->tac_relational->source2->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_relational->source2->tac_identifier->name );
                fprintf(out, "t%d",  tac->tac_relational->source2->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d", tac->tac_relational->source2->tacidx);
            }
            
            fprintf(out, "\n");
            
        }
        
        else if(tac->type == TAC_STACK) {
            if (tac->tac_stack->type == TAC_PUSH){
                fprintf(out, "   push ");
            }
            else if (tac->tac_stack->type == TAC_POP){
                fprintf(out, "   pop ");
            }
            
            
            if(tac->tac_stack->source->type == TAC_CONSTANT) {
                fprintf(out, "t%d",  tac->tac_stack->source->tacidx );
            }
            else if (tac->tac_stack->source->type == TAC_IDENTIFIER) {
                //fprintf(out, "%s",  tac->tac_binary->source1->tac_identifier->name );
                fprintf(out, "t%d",  tac->tac_stack->source->tac_identifier->storage_reg);
            }
            else {
                fprintf(out, "t%d", tac->tac_stack->source->tacidx);
            }
             
            fprintf(out, "\n");
        }
        
        
        else if(tac->type == TAC_LABEL) {
            if (tac->tac_label->name != NULL) {
                // The label has a name, use it!
                fprintf(out, "\n");
                fprintf(out, "_L_%s: \n", tac->tac_label->name);
            }
            else {
                // use the label IDX to form the label name
                fprintf(out, "_L%d: \n", tac->labelidx);
            }
        }
        
        
        else if(tac->type == TAC_CONBRANCH) {
            if (tac->tac_condbranch->operation == TAC_BEQ){
                fprintf(out, "   beq ");
            }
            else if (tac->tac_condbranch->operation == TAC_BNE){
                fprintf(out, "   bne ");
            }
            
            fprintf(out, "t%d, ", tac->tac_condbranch->source->tacidx);
            
            if (tac->tac_condbranch->branch_label->tac_label->name != NULL) {
                // The label has a name, use it!
                fprintf(out, "_L_%s \n", tac->tac_label->name);
            }
            else {
                // use the label IDX to form the label name
                fprintf(out, "_L%d \n", tac->tac_condbranch->branch_label->labelidx);
            }
        }
        
        
        else if(tac->type == TAC_JUMP) {
            
            fprintf(out, "   j ");
            
            if (tac->tac_jump->branch_label->tac_label->name != NULL) {
                // The label has a name, use it!
                fprintf(out, "_L_%s \n", tac->tac_label->name);
            }
            else {
                // use the label IDX to form the label name
                fprintf(out, "_L%d \n", tac->tac_jump->branch_label->labelidx);
            }
        }
        
        
        
        else if(tac->type == TAC_CALL) {
            
            fprintf(out, "   call lr, ");
            if (tac->tac_call->branch_label->tac_identifier->name != NULL) {
                // The label has a name, use it!
                fprintf(out, "_L_%s(r0) \n", tac->tac_call->branch_label->tac_identifier->name);
            }
        }
        
        
        else if(tac->type == TAC_FPACCESS) {
            
            fprintf(out, "   lw ");
            
            if (tac->tac_framepointer_access->destination->type == TAC_IDENTIFIER) {
                
                fprintf(out, "t%d, %d(fp) \n",
                        tac->tac_framepointer_access->destination->tac_identifier->storage_reg,
                        tac->tac_framepointer_access->stack_offset);
            }
        }
        
        
        else if(tac->type == TAC_ENTRYPOINT) {
            
            //fprintf(out, "; Entry point:\n");
            fprintf(out, "   push lr \n");
            fprintf(out, "   push fp \n");
            fprintf(out, "   addui fp, sp, #0 \n\n");
            //fprintf(out, "; Function body:\n");
        }
        
        
        else if(tac->type == TAC_EXITPOINT) {
            
            //fprintf(out, "; Exit point:\n");
            fprintf(out, "   pop fp \n");
            fprintf(out, "   pop lr \n");
            fprintf(out, "   j 0(lr) \n\n\n");
        }
        
        if (tac->type == TAC_GLOBALVAR) {
            
            fprintf(out, "   lw ");
            
            
            if (tac->tac_globalvar->source->type == TAC_IDENTIFIER) {
                
                fprintf(out, "t%d, %s(r0) \n", tac->tac_globalvar->source->tac_identifier->storage_reg,
                        tac->tac_globalvar->source->tac_identifier->name);
            }
        }
        
        
        else if(tac->type == TAC_STACKPOINTER) {
            
            
            if (tac->tac_stack_pointer->type == TAC_SP_DEC){
                fprintf(out, "   subi sp, sp, #%d \n", tac->tac_stack_pointer->constant);
            }
            else if (tac->tac_stack_pointer->type == TAC_SP_INC){
                fprintf(out, "   addi sp, sp, #%d \n", tac->tac_stack_pointer->constant);
            }
        }
        
    }
}


