//
//  stack.h
//  BabyCC
//
//  Created by Patricio Bulic on 01/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#ifndef stack_h
#define stack_h

#include <stdbool.h>
#include "list.h"

/*
 *
 * Stack holds the pointers to Syntacks structures.
 *
 */
typedef struct Stack {
    int size;
    void **content;         // array of pointers to Syntax structures
} Stack;

Stack *stack_new();

void stack_free(Stack *stack);
void stack_push(Stack *stack, void *item);
void *stack_pop(Stack *stack);
void *stack_peek(Stack *stack);
bool stack_empty(Stack *stack);

#endif /* stack_h */
