//
//  AST.c
//  BabyCC
//
//  Created by Patricio Bulic on 03/06/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//

#include "AST.h"
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include "list.h"



ASTnode *ast_type_spec_new(ASTTypeSpecifierType type) {
    ASTTypeSpecifier *ts = malloc(sizeof(ASTTypeSpecifier));
    ts->type = type;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_TYPE_SPECIFIER;
    astnode->type_specifier = ts;

    return astnode;
}


ASTnode *ast_constant_new(int value) {
    ASTConstant *constant = malloc(sizeof(ASTConstant));
    constant->value = value;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_CONSTANT;
    astnode->constant = constant;

    return astnode;
}


ASTnode *ast_identifier_new(char *identifier_name) {
    ASTIdentifier *identifier = malloc(sizeof(ASTIdentifier));
    identifier->identifier_name = identifier_name;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_IDENTIFIER;
    astnode->identifier = identifier;

    return astnode;
}


ASTnode *ast_init_declarator_new(ASTnode *declarator, ASTnode *initializer, ASTInitDeclaratorType type) {
    ASTInitDeclarator *initdecl = malloc(sizeof(ASTInitDeclarator));
    initdecl->type = type;
    initdecl->declarator = declarator;
    initdecl->initializer = initializer;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_INIT_DECLARATOR;
    astnode->init_declarator = initdecl;

    return astnode;
}


ASTnode *ast_unary_new(ASTnode *right, ASTUnaryExpressionType type) {
    ASTUnaryExpression *unary_exp = malloc(sizeof(ASTUnaryExpression));
    unary_exp->type = type;
    unary_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_UNARY_OP;
    astnode->unary_expression = unary_exp;

    return astnode;
}


ASTnode *ast_binary_new(ASTnode *left, ASTnode *right, ASTBinaryExpressionType type) {
    ASTBinaryExpression *binary_exp = malloc(sizeof(ASTBinaryExpression));
    binary_exp->type = type;
    binary_exp->left = left;
    binary_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_BINARY_OP;
    astnode->binary_expression = binary_exp;

    return astnode;
}


ASTnode *ast_relational_new(ASTnode *left, ASTnode *right, ASTRelationalExpressionType type) {
    ASTRelationalExpression *relational_exp = malloc(sizeof(ASTRelationalExpression));
    relational_exp->type = type;
    relational_exp->left = left;
    relational_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_RELATIONAL_OP;
    astnode->relational_expression = relational_exp;

    return astnode;
}


ASTnode *ast_shift_new(ASTnode *left, ASTnode *right, ASTShiftExpressionType type) {
    ASTShiftExpression *shift_exp = malloc(sizeof(ASTShiftExpression));
    shift_exp->type = type;
    shift_exp->left = left;
    shift_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_SHIFT_OP;
    astnode->shift_expression = shift_exp;

    return astnode;
}


ASTnode *ast_assignment_new(ASTnode *left, ASTnode *right, ASTAssignmentExpressionType type) {
    ASTAssignmentExpression *assign_exp = malloc(sizeof(ASTAssignmentExpression));
    assign_exp->type = type;
    assign_exp->left = left;
    assign_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_ASSIGNMENT_EX;
    astnode->assignment_expression = assign_exp;

    return astnode;
}


ASTnode *ast_equality_new(ASTnode *left, ASTnode *right, ASTEqualityExpressionType type) {
    ASTEqualityExpression *equality_exp = malloc(sizeof(ASTEqualityExpression));
    equality_exp->type = type;
    equality_exp->left = left;
    equality_exp->right = right;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_EQAULITY_OP;
    astnode->equality_expression = equality_exp;

    return astnode;
}



ASTnode *ast_jump_statement_new(ASTnode *expression, ASTJumpStatementType type) {
    ASTJumpStatement *jump_statement = malloc(sizeof(ASTJumpStatement));
    jump_statement->expression = expression;
    jump_statement->type = type;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_JUMP_STATEMENT;
    astnode->jump_statement = jump_statement;

    return astnode;
}

ASTnode *ast_declaration_statement_new(ASTnode *specifier, ASTnode *declarator_list, ASTDeclarationStatementType type) {
    ASTDeclarationStatement *ds = malloc(sizeof(ASTDeclarationStatement));
    ds->declarator_list = declarator_list;
    ds->specifier = specifier;
    ds->type = type;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_DECLARATION_STATEMENT;
    astnode->declaration_statement = ds;

    return astnode;
}


ASTnode *ast_parameter_declaration_new(ASTnode *specifier, ASTnode *declarator, ASTDeclarationStatementType type) {
    ASTParameterDeclaration *pd = malloc(sizeof(ASTParameterDeclaration));
    pd->declarator = declarator;
    pd->specifier = specifier;
    pd->type = type;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_PARAMETER_DECLARATION;
    astnode->parameter_declaration = pd;

    return astnode;
}


ASTnode *ast_parameter_list_new(List *parameter_declarations) {
    ASTParametertList *pl = malloc(sizeof(ASTParametertList));
    pl->parameter_declarations = parameter_declarations;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_PARAMETER_LIST;
    astnode->parameter_list = pl;

    return astnode;
}


ASTnode *ast_argument_list_new(List *arguments) {
    ASTArgumentList *al = malloc(sizeof(ASTArgumentList));
    al->arguments = arguments;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_ARGUMENT_LIST;
    astnode->argument_list = al;

    return astnode;
}


ASTnode *ast_declaration_list_new(List *declaration_statements) {
    ASTDeclarationList *dl = malloc(sizeof(ASTDeclarationList));
    dl->declaration_statements = declaration_statements;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_DECLARATION_LIST;
    astnode->declaration_list = dl;

    return astnode;
}



ASTnode *ast_expression_statement_new(ASTnode *expression, ASTExpressionStatementType type) {
    ASTExpressionStatement *exp_statement = malloc(sizeof(ASTExpressionStatement));
    exp_statement->type = type;
    exp_statement->expression = expression;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_EXPRESSION_STATEMENT;
    astnode->expression_statement = exp_statement;

    return astnode;
}


ASTnode *ast_selection_statement_new(ASTnode *condition,
                                     ASTnode *true_statement,
                                     ASTnode *false_statement,
                                     ASTSelectionStatementType type) {
    
    ASTSelectionStatement *sel_statement = malloc(sizeof(ASTSelectionStatement));
    sel_statement->type = type;
    sel_statement->condition = condition;
    sel_statement->true_statement = true_statement;
    sel_statement->false_statement = false_statement;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_SELECTION_STATEMENT;
    astnode->selection_statement = sel_statement;

    return astnode;
}


ASTnode *ast_iteration_statement_new(ASTnode *condition,
                                     ASTnode *statement,
                                     ASTnode *forinit,
                                     ASTnode *forupdate,
                                     ASTIterationStatementType type) {
    
    ASTIterationStatement *it_statement = malloc(sizeof(ASTIterationStatement));
    it_statement->type = type;
    it_statement->condition = condition;
    it_statement->statement = statement;
    it_statement->forinit = forinit;
    it_statement->forupdate = forupdate;
    

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_ITERATION_STATEMENT;
    astnode->iteration_statement = it_statement;

    return astnode;
}


ASTnode *ast_block_new(List *statements) {
    ASTBlock *block = malloc(sizeof(ASTBlock));
    block->statements = statements;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_BLOCK;
    astnode->block = block;

    return astnode;
}



ASTnode *ast_compound_statement_new(ASTnode *block) {
    ASTCompoundStatement *comp_statement = malloc(sizeof(ASTCompoundStatement));
    comp_statement->block = block;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_COMPOUND_STATEMENT;
    astnode->compound_statement = comp_statement;

    return astnode;
}


ASTnode *ast_function_definition_new(ASTnode *declaration_specifiers,
                                     ASTnode *declarator,
                                     ASTnode *compound_statement) {
    
    ASTFunctionDefinition *node = malloc(sizeof(ASTFunctionDefinition));
    node->declaration_specifiers = declaration_specifiers;
    node->declarator = declarator;
    node->compound_statement = compound_statement;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_FUNCTION_DEFINITION;
    astnode->function_definition = node;

    return astnode;
}



ASTnode *ast_function_declarator_new(ASTnode *name,
                                     ASTnode *parameter_type_list,
                                     ASTFunctionDeclaratorType type) {
    
    ASTFunctionDeclarator *fd = malloc(sizeof(ASTFunctionDeclarator));
    fd->type = type;
    fd->func_identifier = name;
    fd->parameter_type_list = parameter_type_list;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_FUNCTION_DECLARATOR;
    astnode->function_declarator = fd;

    return astnode;
}


ASTnode *ast_function_call_new(ASTnode *name,
                               ASTnode *argument_list,
                               ASTFunctionDeclaratorType type) {
    
    ASTFunctionCall *fc = malloc(sizeof(ASTFunctionCall));
    fc->type = type;
    fc->name = name;
    fc->argument_list = argument_list;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_FUNCTION_CALL;
    astnode->function_call = fc;

    return astnode;
}


ASTnode *ast_translational_unit_new(List *ext_declarations) {
    ASTTranslationalUnit *tu = malloc(sizeof(ASTTranslationalUnit));
    tu->ext_declarations = ext_declarations;

    ASTnode *astnode = malloc(sizeof(ASTnode));
    astnode->type = AST_TRANSLATIONAL_UNIT;
    astnode->translational_unit = tu;

    return astnode;
}



void ast_list_free(List *ast_nodes) {
    if (ast_nodes == NULL) {
        return;
    }

    for (int i = 0; i < list_length(ast_nodes); i++) {
        AST_free(list_get(ast_nodes, i));
    }

    list_free(ast_nodes);
}





char* AST_type_name(ASTnode *astnode) {
    if (astnode->type == AST_CONSTANT) {
        return "AST_CONSTANT";
    }
    else if (astnode->type == AST_IDENTIFIER) {
        return "AST_IDENTIFIER";
    }
    else if (astnode->type == AST_TYPE_SPECIFIER) {
        return "AST_TYPE_SPECIFIER";
    }
    else if (astnode->type == AST_UNARY_OP) {
        return "AST_UNARY_OP";
    }
    else if (astnode->type == AST_BINARY_OP) {
        return "AST_BINARY_OP";
    }
    else if (astnode->type == AST_SHIFT_OP) {
        return "AST_SHIFT_OP";
    }
    else if (astnode->type == AST_EQAULITY_OP) {
        return "AST_EQAULITY_OP";
    }
    else if (astnode->type == AST_RELATIONAL_OP) {
        return "AST_RELATIONAL_OP";
    }
    else if (astnode->type == AST_ASSIGNMENT_EX) {
        return "AST_ASSIGNMENT_EX";
    }
    else if (astnode->type == AST_INIT_DECLARATOR) {
        return "AST_INIT_DECLARATOR";
    }
    else if (astnode->type == AST_JUMP_STATEMENT) {
        return "AST_JUMP_STATEMENT";
    }
    else if (astnode->type == AST_DECLARATION_STATEMENT) {
        return "AST_DECLARATION_STATEMENT";
    }
    else if (astnode->type == AST_PARAMETER_DECLARATION) {
        return "AST_PARAMETER_DECLARATION";
    }
    else if (astnode->type == AST_PARAMETER_LIST) {
        return "AST_PARAMETER_LIST";
    }
    else if (astnode->type == AST_ARGUMENT_LIST) {
        return "AST_ARGUMENT_LIST";
    }
    else if (astnode->type == AST_DECLARATION_LIST) {
        return "AST_DECLARATION_LIST";
    }
    else if (astnode->type == AST_EXPRESSION_STATEMENT) {
        return "AST_EXPRESSION_STATEMENT";
    }
    else if (astnode->type == AST_SELECTION_STATEMENT) {
        return "AST_SELECTION_STATEMENT";
    }
    else if (astnode->type == AST_ITERATION_STATEMENT) {
        return "AST_ITERATION_STATEMENT";
    }
    else if (astnode->type == AST_BLOCK) {
        return "AST_BLOCK";
    }
    else if (astnode->type == AST_COMPOUND_STATEMENT) {
        return "AST_COMPOUND_STATEMENT";
    }
    else if (astnode->type == AST_FUNCTION_DEFINITION) {
        return "AST_FUNCTION_DEFINITION";
    }
    
    else if (astnode->type == AST_FUNCTION_DECLARATOR) {
        return "AST_FUNCTION_DECLARATOR";
    }
    else if (astnode->type == AST_FUNCTION_CALL) {
        return "AST_FUNCTION_CALL";
    }
    else if (astnode->type == AST_TRANSLATIONAL_UNIT) {
        return "AST_TRANSLATIONAL_UNIT";
    }
    
    
    // we should not get here
    return "??? UNKNOWN AST NODE TYPE ???";
}



void AST_free(ASTnode *astnode) {
    if (astnode->type == AST_CONSTANT) {
        free(astnode->constant);
    }
    else if (astnode->type == AST_TYPE_SPECIFIER) {
        free(astnode->type_specifier);
    }
    else if (astnode->type == AST_IDENTIFIER) {
        free(astnode->identifier);
    }
    else if (astnode->type == AST_UNARY_OP) {
        AST_free(astnode->unary_expression->right);
        free(astnode->unary_expression);
    }
    else if (astnode->type == AST_BINARY_OP) {
        AST_free(astnode->binary_expression->left);
        AST_free(astnode->binary_expression->right);
        free(astnode->binary_expression);
    }
    else if (astnode->type == AST_SHIFT_OP) {
        AST_free(astnode->shift_expression->left);
        AST_free(astnode->shift_expression->right);
        free(astnode->shift_expression);
    }
    else if (astnode->type == AST_EQAULITY_OP) {
        AST_free(astnode->equality_expression->left);
        AST_free(astnode->equality_expression->right);
        free(astnode->equality_expression);
    }
    else if (astnode->type == AST_RELATIONAL_OP) {
        AST_free(astnode->relational_expression->left);
        AST_free(astnode->relational_expression->right);
        free(astnode->relational_expression);
    }
    else if (astnode->type == AST_ASSIGNMENT_EX) {
        AST_free(astnode->assignment_expression->left);
        AST_free(astnode->assignment_expression->right);
        free(astnode->assignment_expression);
    }
    else if (astnode->type == AST_INIT_DECLARATOR) {
        AST_free(astnode->init_declarator->declarator);
        AST_free(astnode->init_declarator->initializer);
        free(astnode->init_declarator);
    }
    else if (astnode->type == AST_JUMP_STATEMENT) {
        AST_free(astnode->jump_statement->expression);
        free(astnode->jump_statement);
    }
    else if (astnode->type == AST_EXPRESSION_STATEMENT) {
        AST_free(astnode->expression_statement->expression);
        free(astnode->expression_statement);
    }
    else if (astnode->type == AST_SELECTION_STATEMENT) {
        AST_free(astnode->selection_statement->condition);
        AST_free(astnode->selection_statement->true_statement);
        AST_free(astnode->selection_statement->false_statement);
        free(astnode->selection_statement);
    }
    else if (astnode->type == AST_ITERATION_STATEMENT) {
        AST_free(astnode->iteration_statement->condition);
        AST_free(astnode->iteration_statement->statement);
        free(astnode->iteration_statement);
    }
    
    else if (astnode->type == AST_DECLARATION_STATEMENT) {
        AST_free(astnode->declaration_statement->specifier);
        AST_free(astnode->declaration_statement->declarator_list);
        free(astnode->declaration_statement);
    }
    else if (astnode->type == AST_PARAMETER_DECLARATION) {
        AST_free(astnode->parameter_declaration->specifier);
        AST_free(astnode->parameter_declaration->declarator);
        free(astnode->parameter_declaration);
    }
    else if (astnode->type == AST_PARAMETER_LIST) {
        ast_list_free(astnode->parameter_list->parameter_declarations);
        free(astnode->parameter_list);
    }
    else if (astnode->type == AST_ARGUMENT_LIST) {
        ast_list_free(astnode->argument_list->arguments);
        free(astnode->argument_list);
    }
    else if (astnode->type == AST_DECLARATION_LIST) {
        ast_list_free(astnode->declaration_list->declaration_statements);
        free(astnode->declaration_list);
    }
    
    else if (astnode->type == AST_BLOCK) {
        ast_list_free(astnode->block->statements);
        free(astnode->block);
    }
    else if (astnode->type == AST_COMPOUND_STATEMENT) {
        AST_free(astnode->compound_statement->block);
        free(astnode->compound_statement);
    }
    else if (astnode->type == AST_FUNCTION_DEFINITION) {
        AST_free(astnode->function_definition->declaration_specifiers);
        AST_free(astnode->function_definition->declarator);
        AST_free(astnode->function_definition->compound_statement);
        free(astnode->function_definition);
    }
    else if (astnode->type == AST_FUNCTION_DECLARATOR) {
        AST_free(astnode->function_declarator->func_identifier);
        AST_free(astnode->function_declarator->parameter_type_list);
        free(astnode->function_declarator);
    }
    else if (astnode->type == AST_FUNCTION_CALL) {
        AST_free(astnode->function_call->name);
        AST_free(astnode->function_call->argument_list);
        free(astnode->function_call);
    }
    else if (astnode->type == AST_TRANSLATIONAL_UNIT) {
        ast_list_free(astnode->translational_unit->ext_declarations);
        free(astnode->translational_unit);
    }
    else {
        warnx("Unknown AST node %s", AST_type_name(astnode));
    }
    
    free(astnode);
}





void AST_dump(FILE* out, ASTnode *astnode) {
    if (astnode->type == AST_CONSTANT) {
        fprintf(out, "%d", astnode->constant->value);
    }
    else if (astnode->type == AST_IDENTIFIER) {
        fprintf(out, "%s", astnode->identifier->identifier_name);
    }
    else if (astnode->type == AST_TYPE_SPECIFIER) {
        ASTTypeSpecifier *ast = astnode->type_specifier;
        if (ast->type == TS_INT) {
            fprintf(out, ".WORD ");
        }
        else if (ast->type == TS_BOOL) {
            fprintf(out, ".BYTE ");
        }
        else if (ast->type == TS_CHAR) {
            fprintf(out, ".ASCII ");
        }
        else if (ast->type == TS_SHORT) {
            fprintf(out, ".HALF ");
        }
        else {
            /*TODO: add other types*/
            fprintf(out, ".SOMETYPE ");
        }
        
    }
    
    else if (astnode->type == AST_UNARY_OP) {
        ASTUnaryExpression *ast = astnode->unary_expression;
        
        if (ast->type == UE_PLUS) {
            fprintf(out, "+(");
        }
        else if (ast->type == UE_MINUS) {
            fprintf(out, "-(");
        }
        else if (ast->type == UE_LOGNEG) {
            fprintf(out, "!(");
        }
        else if (ast->type == UE_ADDRESS) {
            fprintf(out, "&(");
        }
        else if (ast->type == UE_BITWISENEG) {
            fprintf(out, "~(");
        }
        else if (ast->type == UE_DEREFERENCE) {
            fprintf(out, "*(");
        }
        
        
        AST_dump(out, ast->right);
        fprintf(out, ")");
    }
    
    else if (astnode->type == AST_BINARY_OP) {
        ASTBinaryExpression *ast = astnode->binary_expression;
        if (ast->type == BE_MULTIPLICATION) {
            fprintf(out, "MUL(");
        }
        else if (ast->type == BE_ADDITION) {
            fprintf(out, "ADD(");
        }
        else if (ast->type == BE_SUBTRACTION) {
            fprintf(out, "SUB(");
        }
        else if (ast->type == BE_DIVISION) {
            fprintf(out, "DIV(");
        }
        else if (ast->type == BE_BIN_OR) {
            fprintf(out, "BIN_OR(");
        }
        else if (ast->type == BE_BIN_AND) {
            fprintf(out, "BIN_AND(");
        }
        else if (ast->type == BE_BIN_XOR) {
            fprintf(out, "BIN_XOR(");
        }
        else if (ast->type == BE_LOG_OR) {
            fprintf(out, "LOG_OR(");
        }
        else if (ast->type == BE_LOG_AND) {
            fprintf(out, "LOG_AND(");
        }
        
        AST_dump(out, ast->left);
        fprintf(out, ", ");
        AST_dump(out, ast->right);
        fprintf(out, ")");
        
    }
    else if (astnode->type == AST_SHIFT_OP) {
        ASTShiftExpression *ast = astnode->shift_expression;
        if (ast->type == SE_LEFT) {
            fprintf(out, "SHL(");
        }
        else if (ast->type == SE_RIGHT) {
            fprintf(out, "SHR(");
        }
        
        AST_dump(out, ast->left);
        fprintf(out, ", ");
        AST_dump(out, ast->right);
        fprintf(out, ")");
        
    }
    else if (astnode->type == AST_EQAULITY_OP) {
        ASTEqualityExpression *ast = astnode->equality_expression;
        if (ast->type == EE_EQUAL) {
            fprintf(out, "EQ?(");
        }
        else if (ast->type == EE_NOT_EQUAL) {
            fprintf(out, "NEQ?(");
        }
        
        AST_dump(out, ast->left);
        fprintf(out, ", ");
        AST_dump(out, ast->right);
        fprintf(out, ")");
        
    }
    else if (astnode->type == AST_RELATIONAL_OP) {
        ASTRelationalExpression *ast = astnode->relational_expression;
        if (ast->type == RE_LESS_THAN) {
            fprintf(out, "LT(");
        }
        else if (ast->type == RE_LESS_THAN_OR_EQUAL) {
            fprintf(out, "LTE(");
        }
        else if (ast->type == RE_GREATER_THAN) {
            fprintf(out, "GT(");
        }
        else if (ast->type == RE_GREATER_THAN_OR_EQUAL) {
            fprintf(out, "GTE(");
        }
        
        AST_dump(out, ast->left);
        fprintf(out, ", ");
        AST_dump(out, ast->right);
        fprintf(out, ")");
        
    }
    else if (astnode->type == AST_ASSIGNMENT_EX) {
        ASTAssignmentExpression *ast = astnode->assignment_expression;
        AST_dump(out, ast->left);
        fprintf(out, " = ");
        
        if (ast->type == AE_ASSIGN) {
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_OR_ASSIGN) {
            AST_dump(out, ast->left);
            fprintf(out, " | (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_ADD_ASSIGN) {
            AST_dump(out, ast->left);
            fprintf(out, " + (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_AND_ASSIGN) {
            AST_dump(out, ast->left);
            fprintf(out, " & (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_DIV_ASSIGN) {
            AST_dump(out, ast->left);
            fprintf(out, " / (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_MOD_ASSIGN) {
            AST_dump(out, ast->left);
            fprintf(out, " %% (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_SUB_ASSIGN) {
            fprintf(out, " - (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_XOR_ASSIGN) {
            fprintf(out, " ^ (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_LEFT_ASSIGN) {
            fprintf(out, " << (");
            AST_dump(out, ast->right);
        }
        else if (ast->type == AE_RIGHT_ASSIGN) {
            fprintf(out, " >> (");
            AST_dump(out, ast->right);
        }
        fprintf(out, ")");
    
    }
    else if (astnode->type == AST_INIT_DECLARATOR) {
        ASTInitDeclarator *ast = astnode->init_declarator;
        
        if (ast->type == ID_NOINIT) {
            AST_dump(out, ast->declarator);
        }
        else if (ast->type == ID_INIT) {
            AST_dump(out, ast->declarator);
            fprintf(out, " = ");
            AST_dump(out, ast->initializer);
        }
        
    }
    
    else if (astnode->type == AST_JUMP_STATEMENT) {
        ASTJumpStatement *ast = astnode->jump_statement;
        if (ast->type == JS_RETURN_EXP) {
            fprintf(out, "RET(");
            AST_dump(out, ast->expression);
            fprintf(out, ")");
        }
        else if (ast->type == JS_GOTO) {
            
        }
        else if (ast->type == JS_BREAK) {
            
        }
        else if (ast->type == JS_CONTINUE) {
            
        }
        fprintf(out, "\n");
    }
    
    else if (astnode->type == AST_ITERATION_STATEMENT) {
        ASTIterationStatement *ast = astnode->iteration_statement;
        if (ast->type == IS_WHILE) {
            fprintf(out, "WHILE(");
            AST_dump(out, ast->condition);
            fprintf(out, ") { \n");
            AST_dump(out, ast->statement);
            fprintf(out, "}");
        }
        else if (ast->type == IS_DO) {
            /*TODO: */
        }
        else if (ast->type == IS_FOR) {
            /*TODO: */
        }
        fprintf(out, "\n");
    }
    
    
    else if (astnode->type == AST_SELECTION_STATEMENT) {
        ASTSelectionStatement *ast = astnode->selection_statement;
        if (ast->type == SS_IF) {
            /*TODO: */
        }
        else if (ast->type == SS_IFELSE) {
            /*TODO: */
        }
        else if (ast->type == SS_SWITCH) {
            /*TODO: */
        }
        fprintf(out, "\n");
    }
    
    else if (astnode->type == AST_DECLARATION_STATEMENT) {
        ASTDeclarationStatement *ast = astnode->declaration_statement;
        
        if (ast->type == DS_LOCAL) {
            fprintf(out, "LOCAL ");
        }
        else if (ast->type == DS_GLOBAL) {
            fprintf(out, "GLOBAL ");
        }
        if (ast->type == DS_ARGUMENTS) {
            fprintf(out, "ARGUMENT ");
        }
        
        AST_dump(out, ast->specifier);
        fprintf(out, " ");
        AST_dump(out, ast->declarator_list);
        fprintf(out, "\n");
        
    }
    
    else if (astnode->type == AST_PARAMETER_DECLARATION) {
        ASTParameterDeclaration *ast = astnode->parameter_declaration;
        
        if (ast->type == DS_LOCAL) {
            fprintf(out, "LOCAL ");
        }
        else if (ast->type == DS_GLOBAL) {
            fprintf(out, "GLOBAL ");
        }
        if (ast->type == DS_ARGUMENTS) {
            fprintf(out, "ARGUMENT ");
        }
        
        AST_dump(out, ast->specifier);
        fprintf(out, " ");
        AST_dump(out, ast->declarator);
        fprintf(out, "\n");
        
    }
    
    else if (astnode->type == AST_DECLARATION_LIST) {
        ASTDeclarationList *ast = astnode->declaration_list;
        /*
         * We should dump all declaration statements in a ddeclaration list
         */
        List *declaration_statements = ast->declaration_statements;
        
        for (int i = list_length(declaration_statements)-1; i >= 0; i--) {
            AST_dump(out, list_get(declaration_statements, i));
        }
        
    }
    
    else if (astnode->type == AST_EXPRESSION_STATEMENT) {
        ASTExpressionStatement *ast = astnode->expression_statement;
        if (ast->type == ES_EXPRESSION) {
            AST_dump(out, ast->expression);
            // just new line to designate the statement
            fprintf(out, ";\n");
        }
    }
    
    else if (astnode->type == AST_BLOCK) {
        ASTBlock *ast = astnode->block;
        /*
         * We should dump all statements in a Block's list
         */
        List *statements = ast->statements;
        
        for (int i = list_length(statements)-1; i >= 0; i--) {
            AST_dump(out, list_get(statements, i));
        }
        
    }
    
    else if (astnode->type == AST_COMPOUND_STATEMENT) {
        ASTCompoundStatement *ast = astnode->compound_statement;
        fprintf(out, "<<< \n");
        AST_dump(out, ast->block);
        fprintf(out, ">>> \n");
    }
    
    
    else if (astnode->type == AST_FUNCTION_DEFINITION) {
        ASTFunctionDefinition *ast = astnode->function_definition;
        //AST_dump(out, ast->declaration_specifiers);
        fprintf(out, "\n");
        AST_dump(out, ast->declarator);
        fprintf(out, ": \n");
        AST_dump(out, ast->compound_statement);
    }
    
    else if (astnode->type == AST_FUNCTION_DECLARATOR) {
        ASTFunctionDeclarator *ast = astnode->function_declarator;
        //AST_dump(out, ast->declaration_specifiers);
        fprintf(out, "\n");
        AST_dump(out, ast->func_identifier);
        fprintf(out, ": \n");
        AST_dump(out, ast->parameter_type_list);
    }
    
    
    else if (astnode->type == AST_FUNCTION_CALL) {
        ASTFunctionCall *ast = astnode->function_call;
    
        fprintf(out, "CALL (");
        if(ast->type == FD_NOARGS) {
            AST_dump(out, ast->name);
        }
        else if(ast->type == FD_ARGS) {
            AST_dump(out, ast->name);
            fprintf(out, "(");
            AST_dump(out, ast->argument_list);
            fprintf(out, ")");
        }
        fprintf(out, ")\n");
    }
    
    
    else if (astnode->type == AST_TRANSLATIONAL_UNIT) {
        
    }
    else {
        warnx("DUMP ERROR: Unknown AST node %s", AST_type_name(astnode));
    }
    
}



void help (ASTnode *declarator) {
    
    
    //ASTNodeType nodeType = declarator->type;
    //assert(declarator->type == AST_FUNCTION_DECLARATOR);
    //assert(declarator->function_declarator->func_identifier->type == AST_IDENTIFIER);
    //char* funcname = declarator->function_declarator->func_identifier->identifier->identifier_name;
    
    //printf("+++%s+++ \n", funcname);
    
}

