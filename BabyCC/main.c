//
//  main.c
//  BabyCC
//
//  Created by Patricio Bulic on 31/05/2020.
//  Copyright © 2020 Patricio Bulic. All rights reserved.
//


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <err.h>
#include "y.tab.h"
#include "stack.h"
#include "AST.h"
#include "symbol_table.h"
#include "emmit.h"
#include "tac.h"

extern int yyparse(void);

extern FILE *yyin;
FILE *fProductions;
extern Stack *syntax_stack;
extern ASTnode* imm_syn;

extern int number_of_blocks;

int current_scope = 0;
int current_scope_length = 0;
int current_tacidx = 0;
int current_labelidx = 0;
int current_stack_offset = 0;
int current_args_stack_offset = 0;

SymbolTable* symtab;
TAC* tac;
List* tac_list_emmit;       // this list contains created TACs thet are emmmited.
List* tac_list_noemmit;     // this list contains created TACs thet are not emmmited. We need this list to free this TACs
List* dataseg_tac_list;          // this list contains created TACs thet belong to data segment


int main(int argc, const char * argv[]) {
    
    ++argv, --argc;  // Skip over program name.
    if (argc > 0) {
        yyin = fopen(argv[0], "r");
        if (yyin == NULL) {
            printf("Cannot open file!\n");
            return -1;
        }
    } else {
        printf("No input file!\n");
        return -1;
    }
    
    
    number_of_blocks = 0;
    syntax_stack = stack_new();
    symtab = symbol_table_new();
    tac_list_emmit = list_new();
    tac_list_noemmit = list_new();
    dataseg_tac_list = list_new();
    
    
    /*
     ********************************************************************************************
     *     B U I L D   A S T  :
     ********************************************************************************************
     */
    fProductions = fopen("/Users/patriciobulic/Delo/FRI/Pedagosko/BabyCC/output/productions.txt", "wb");
    yyparse();
    
    
    
    
    
    /*
    ********************************************************************************************
    *     P A R S E   A S T :  B U I L D  S Y M T A B   A N D  T A C  L I S T
    ********************************************************************************************
    */
    
    int stack_size = syntax_stack->size;
    printf("Input parsed:\n");
    printf("   stack size = %d\n", stack_size);
    printf("   number of blocks = %d\n", number_of_blocks);
    
    /*
    FILE *fAST = fopen("/Users/patriciobulic/Delo/FRI/Pedagosko/BabyCC/AST.txt", "wb");
    while(!stack_empty(syntax_stack)) {
        ASTnode *top_AST = stack_pop(syntax_stack);
        AST_dump(fAST, top_AST);
        //AST_free(top_AST);
    }
    fclose(fAST);
     */
     
    
    printf("Parsing AST ...\n");
    
    FILE *fEMMIT = fopen("/Users/patriciobulic/Delo/FRI/Pedagosko/BabyCC/output/AST.txt", "wb");
    while(!stack_empty(syntax_stack)) {
        ASTnode *top_AST = stack_pop(syntax_stack);
        TAC* top;
        AST_parse(fEMMIT, top_AST, &top);
        //AST_free(top_AST);
    }
    fclose(fEMMIT);
    
    
    /*
    ********************************************************************************************
    *     E M M I T   T A C s :
    ********************************************************************************************
    */
    
    FILE *fTAC = fopen("/Users/patriciobulic/Delo/FRI/Pedagosko/BabyCC/output/tac.s", "wb");
    
    dump_dataseg_TAC_list(fTAC, dataseg_tac_list);
    
    dump_TAC_list(fTAC, tac_list_emmit);
     
    
    
    //dump_const_ident_TAC_list(tac_list_noemmit);
    
    
    /*
     ********************************************************************************************
     *     C L E A N U P :
     ********************************************************************************************
     */
    
    
    while(!stack_empty(syntax_stack)) {
        ASTnode *top_AST = stack_pop(syntax_stack);
        AST_free(top_AST);
    }
    
    stack_free(syntax_stack);
    symbol_table_free(symtab);
    
    free_tac_list(tac_list_emmit);
    free_tac_list(tac_list_noemmit); 
    
    if (yyin != NULL) {
        fclose(yyin);
    }
    
    printf("Successfully compiled. \n");
    
    return 0;
}
