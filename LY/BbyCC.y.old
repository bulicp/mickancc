
%{
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "stack.h"
#include "syntax.h"
#include "list.h"


#define YYSTYPE char*

int yyparse(void);
int yylex();

void yyerror(const char *str)
{
    fprintf(stderr,"error: %s\n",str);
}

int yywrap()
{
    return 1;
}

extern FILE *yyin;
extern char yytext[];
extern int column;

Stack *syntax_stack;
Syntax* imm_syn;

%}





%token INCLUDE HEADER_NAME
%token TYPE IDENTIFIER RETURN NUMBER
%token OPEN_BRACE CLOSE_BRACE
%token IF WHILE
%token LESS_OR_EQUAL


/* Operator associativity, least precedence first.
 * See http://en.cppreference.com/w/c/language/operator_precedence
 */
%left '='
%left '<'
%left '+'
%left '-'
%left '*'
%nonassoc '!'
%nonassoc '~'

%start program

%%




program
    : function
    | program function
    ;

function
    : TYPE IDENTIFIER '(' ')' '{' block '}'
    ;


block:
    statement
    {
        /* The first statement forms the block */
        
        /* Append to the current block, or start a new block. */
        Syntax *block_syntax;
        
        assert(!stack_empty(syntax_stack));
        // Top of the stack contains a statement. POP it.
        Syntax *top_statement = stack_pop(syntax_stack);
        
        /*
         * Check what follows on the stack (PEEP stack)
         * If there is no block, create a new one with an empty list
         */
        if (stack_empty(syntax_stack)) {
            /* The stack could be empty here if this is the first var declaration ??? */
            block_syntax = block_new(list_new());
        }
        else if (((Syntax *)stack_peek(syntax_stack))->type != BLOCK) {
            /* stack contains at least one statement*/
            /* create a new block with empty list */
            block_syntax = block_new(list_new());
            
            
        /*
         * otherwise POP the block from the top of stack
         */
        } else {  // POP the block from the stack
            block_syntax = stack_pop(syntax_stack);
        }

        /*
         * Now, add the new statement from top of the stack to the begining of the list
         * (PUSH statement) and push the new block onto stack
         */
        
        
        list_push(block_syntax->block->statements, top_statement);
        //list_push(block_syntax->block->statements, stack_pop(syntax_stack));
        stack_push(syntax_stack, block_syntax);
    }
    | block statement
    {
        /* Append to the current block, or start a new block. */
        Syntax *block_syntax;
        
        assert(!stack_empty(syntax_stack));
        // Top of the stack contains a statement. POP it.
        Syntax *top_statement = stack_pop(syntax_stack);
        
        /*
         * Check what follows on the stack (PEEP stack)
         * If there is no block, create a new one with an empty list
         */
        if (stack_empty(syntax_stack)) {
            /* TODO: Actually, the stack should not be empty upon arrival to this point */
            block_syntax = block_new(list_new());
        }
        else if (((Syntax *)stack_peek(syntax_stack))->type != BLOCK) {
            /* create a new block with empty list */
            block_syntax = block_new(list_new());
            
        /*
         * otherwise POP the block from the top of stack
         */
        } else {  // POP the block from the stack
            block_syntax = stack_pop(syntax_stack);
        }

        /*
         * Now, add the new statement from top of the stack to the begining of the list
         * (PUSH statement) and push the new block onto stack
         */
        
        list_push(block_syntax->block->statements, top_statement);
        stack_push(syntax_stack, block_syntax);
    }
    ;
    
statement
    : RETURN expression ';'
    {
        Syntax *current_syntax = stack_pop(syntax_stack);
        stack_push(syntax_stack, return_statement_new(current_syntax));
    }
    | IF '(' expression ')' '{' block '}'
    {
        
    }
    | WHILE '(' expression ')' '{' block '}'
    {
        
    }
    | TYPE IDENTIFIER ';'
    {
        /*TODO: update define_var_new function and structure to denote vars without init value */
        //Syntax *new_var = stack_pop(syntax_stack);
        //if (new_var->type == VARIABLE) {
            //printf("VAR\n");
        //}
        
        stack_push(syntax_stack, declare_var_new((char*)$2));
    }
    | TYPE IDENTIFIER '=' expression ';'
    {
        Syntax *define_var = stack_pop(syntax_stack);
        stack_push(syntax_stack, define_var_new((char*)$2, define_var));
    }
    | expression ';'
    {
        
    }

    ;

expression
    : NUMBER
    {
        //printf("Expression\n");
        //printf("Jebeni $1 = %d", atoi($1));
        imm_syn = immediate_new(atoi((char*)$1));
        stack_push(syntax_stack, imm_syn);
        //stack_push(syntax_stack, immediate_new(atoi((char*)$1)));
        //free($1);
    }
    | IDENTIFIER
    {
        stack_push(syntax_stack, variable_new((char*)$1));
    }
    | IDENTIFIER '=' expression
    {
        //printf("assignment \n");
        Syntax *expression = stack_pop(syntax_stack);
        stack_push(syntax_stack, assignment_new((char*)$1, expression));
    }
    | '~' expression
    | '!' expression
    | expression '+' expression
    {
        //printf("addition \n");
        Syntax *right = stack_pop(syntax_stack);
        Syntax *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, addition_new(left, right));
    }
    | expression '-' expression
    {
        Syntax *right = stack_pop(syntax_stack);
        Syntax *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, subtraction_new(left, right));
    }
    | expression '*' expression
    {
        Syntax *right = stack_pop(syntax_stack);
        Syntax *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, multiplication_new(left, right));
    }
    | expression '<' expression
    {
        Syntax *right = stack_pop(syntax_stack);
        Syntax *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, less_than_new(left, right));
    }
    | expression LESS_OR_EQUAL expression
    {
        Syntax *right = stack_pop(syntax_stack);
        Syntax *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, less_or_equal_new(left, right));
    }
    ;





