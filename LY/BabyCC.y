%{
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "stack.h"
#include "AST.h"
#include "list.h"
#include "list.h"
#include "symbol_table.h"


#define YYSTYPE char*

extern int column;

int yyparse(void);
int yylex();

void yyerror(const char *s)
{
    //fprintf(stderr,"error: %s\n",str);
    fflush(stdout);
    printf("\n%*s\n%*s\n", column, "^", column, s);
}

int yywrap()
{
    return 1;
}

void not_supported(){
    printf("Syntax error!!!!\n");
    //assert(false);
    exit(EXIT_FAILURE);
}

extern FILE *yyin;
extern char yytext[];


extern int current_register;
extern int current_scope;
extern int current_scope_length;
extern int current_tacidx;
extern int current_labelidx;
extern int current_stack_offset;
extern SymbolTable* symtab;
extern List* tac_list;



ASTAssignmentExpressionType AST_ex_type;
ASTUnaryExpressionType AST_unenx_type;

Stack *syntax_stack;
ASTnode* imm_syn;
int number_of_blocks=0;

extern FILE *fProductions;
%}




%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER INLINE RESTRICT INCLUDE
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token BOOL COMPLEX IMAGINARY
%token STRUCT UNION ENUM ELLIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%start translation_unit
%%



primary_expression
    : IDENTIFIER
    {
        fprintf(fProductions, "primary_expression : IDENTIFIER (%s) \n", $1);
        stack_push(syntax_stack, ast_identifier_new((char*)$1));
    }
    | CONSTANT
    {
        fprintf(fProductions, "primary_expression : CONSTANT (%s) \n", $1);
        stack_push(syntax_stack, ast_constant_new(atoi((char*)$1)));
        free($1);
    }
    | STRING_LITERAL
    {
        /*TODO: */
        fprintf(fProductions, "primary_expression : STRING_LITERAL (%s) \n", $1);
        not_supported();
        
    }
    | '(' expression ')'
    {
        /*TODO: */
        fprintf(fProductions, "primary_expression : '(' expression ')' \n");
        not_supported();
    }
    ;

postfix_expression
    : primary_expression
    {
        fprintf(fProductions, "postfix_expression: primary_expression \n");
    }
    | postfix_expression '[' expression ']'
    {
        fprintf(fProductions, "postfix_expression: postfix_expression '[' expression ']' \n");
        not_supported();
    }
    | postfix_expression '(' ')' {
        
        fprintf(fProductions, "func call NOARGS \n");
        ASTnode *name = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_function_call_new(name, NULL, FD_NOARGS));
        
    }
    | postfix_expression '(' argument_expression_list ')'
    {
        //printf(" func call ARGS \n");
        fprintf(fProductions, " postfix_expression : postfix_expression '(' argument_expression_list ')' \n");
        
        
        ASTnode *argument_list = stack_pop(syntax_stack);
        ASTnode *name = stack_pop(syntax_stack);
        
        stack_push(syntax_stack, ast_function_call_new(name, argument_list, FD_ARGS));
    }
    | postfix_expression '.' IDENTIFIER {not_supported();}
    | postfix_expression PTR_OP IDENTIFIER {not_supported();}
    | postfix_expression INC_OP
    {
        fprintf(fProductions, "postfix_expression: postfix_expression INC_OP \n");
        ASTnode *left = stack_pop(syntax_stack);
        ASTnode *right = ast_constant_new(1);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_ADDITION));
    }
    | postfix_expression DEC_OP
    {
        fprintf(fProductions, "postfix_expression: postfix_expression DEC_OP \n");
        ASTnode *left = stack_pop(syntax_stack);
        ASTnode *right = ast_constant_new(1);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_SUBTRACTION));
    }
    | '(' type_name ')' '{' initializer_list '}' {not_supported();}
    | '(' type_name ')' '{' initializer_list ',' '}' {not_supported();}
    ;

argument_expression_list
    : assignment_expression
    {
        fprintf(fProductions, "argument_expression_list: assignment_expression \n");
        assert(!stack_empty(syntax_stack));
        // Pop the function argument :
        ASTnode *argument = stack_pop(syntax_stack);
        
        //create a new argument list:
        ASTnode *ast_al = ast_argument_list_new(list_new());

        /*
         * Now, add the argumentto the begining of the list
         * (PUSH statement) and push the new list onto stack
         */
        
        list_push(ast_al->argument_list->arguments, argument);
        stack_push(syntax_stack, ast_al);
    }
    | argument_expression_list ',' assignment_expression
    {
        fprintf(fProductions, "argument_expression_list: argument_expression_list ',' assignment_expression \n");
        assert(!stack_empty(syntax_stack));
        // Pop the function argument :
        ASTnode *argument = stack_pop(syntax_stack);
        
        // now pop the argument list from the stack:
        ASTnode *ast_al = stack_pop(syntax_stack);

        /*
        * Now, add the argument to the begining of the list
        * (PUSH statement) and push the list onto stack
        */
        
        list_push(ast_al->argument_list->arguments, argument);
        stack_push(syntax_stack, ast_al);
    }
    ;

unary_expression
    : postfix_expression
    {
        fprintf(fProductions, "unary_expression: postfix_expression \n");
    }
    | INC_OP unary_expression
    {
        fprintf(fProductions, "unary_expression: INC_OP unary_expression \n");
        /*TODO: correct semantics and AST. */
        ASTnode *left = stack_pop(syntax_stack);
        ASTnode *right = ast_constant_new(1);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_ADDITION));
    }
    | DEC_OP unary_expression
    {
        fprintf(fProductions, "unary_expression: DEC_OP unary_expression \n");
        /*TODO: correct semantics and AST. */
        ASTnode *left = stack_pop(syntax_stack);
        ASTnode *right = ast_constant_new(1);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_SUBTRACTION));
    }
    | unary_operator cast_expression
    {
        
        fprintf(fProductions, "unary_expression: unary_operator cast_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_unary_new(right, AST_unenx_type));
    }
    | SIZEOF unary_expression {not_supported();}
    | SIZEOF '(' type_name ')' {not_supported();}
    ;

unary_operator
    : '&' { AST_unenx_type = UE_ADDRESS; }
    | '*' { AST_unenx_type = UE_DEREFERENCE; }
    | '+' { AST_unenx_type = UE_PLUS; }
    | '-' { AST_unenx_type = UE_MINUS; }
    | '~' { AST_unenx_type = UE_BITWISENEG; }
    | '!' { AST_unenx_type = UE_LOGNEG; }
    ;

cast_expression
    : unary_expression
    {
        fprintf(fProductions, "cast_expression: unary_expression \n");
    }
    | '(' type_name ')' cast_expression {not_supported();}
    ;

multiplicative_expression
    : cast_expression
    {
        fprintf(fProductions, "multiplicative_expression: cast_expression \n");
        //ASTnode *left = stack_pop(syntax_stack);
        //stack_push(syntax_stack, ast_binary_new(left, NULL, BE_LEFT_ONLY));
    }
    | multiplicative_expression '*' cast_expression
    {
        fprintf(fProductions, "multiplicative_expression '*' cast_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_MULTIPLICATION));
    }
    | multiplicative_expression '/' cast_expression
    {
        fprintf(fProductions, "multiplicative_expression '/' cast_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_DIVISION));
    }
    | multiplicative_expression '%' cast_expression
    {
        fprintf(fProductions, "multiplicative_expression '/' cast_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_MODULUS));
    }
    ;

additive_expression
    : multiplicative_expression
    {
        fprintf(fProductions, "additive_expression: multiplicative_expression \n");
        //ASTnode *left = stack_pop(syntax_stack);
        //stack_push(syntax_stack, ast_binary_new(left, NULL, BE_LEFT_ONLY));
    }
    | additive_expression '+' multiplicative_expression
    {
        fprintf(fProductions, "additive_expression: additive_expression '+' multiplicative_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_ADDITION));
    }
    | additive_expression '-' multiplicative_expression
    {
        fprintf(fProductions, "additive_expression: additive_expression '-' multiplicative_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_SUBTRACTION));
    }
    ;

shift_expression
    : additive_expression
    {
        fprintf(fProductions, "shift_expression: additive_expression \n");
    }
    | shift_expression LEFT_OP additive_expression
    {
        fprintf(fProductions, "shift_expression: shift_expression LEFT_OP additive_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_shift_new(left, right, SE_LEFT));
    }
    | shift_expression RIGHT_OP additive_expression
    {
        fprintf(fProductions, "shift_expression: shift_expression RIGHT_OP additive_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_shift_new(left, right, SE_RIGHT));
    }
    ;

relational_expression
    : shift_expression
    {
        fprintf(fProductions, "relational_expression: shift_expression \n");
    }
    | relational_expression '<' shift_expression
    {
        fprintf(fProductions, "relational_expression: relational_expression < shift_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_relational_new(left, right, RE_LESS_THAN));
    }
    | relational_expression '>' shift_expression
    {
        fprintf(fProductions, "relational_expression: relational_expression > shift_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_relational_new(left, right, RE_GREATER_THAN));
    }
    | relational_expression LE_OP shift_expression
    {
        fprintf(fProductions, "relational_expression: relational_expression LE_OP shift_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_relational_new(left, right, RE_LESS_THAN_OR_EQUAL));
    }
    | relational_expression GE_OP shift_expression
    {
        fprintf(fProductions, "relational_expression: relational_expression GE_OP shift_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_relational_new(left, right, RE_GREATER_THAN_OR_EQUAL));
    }
    ;

equality_expression
    : relational_expression
    {
        fprintf(fProductions, "equality_expression: relational_expression \n");
    }
    | equality_expression EQ_OP relational_expression
    {
        fprintf(fProductions, "equality_expression: equality_expression EQ_OP relational_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_equality_new(left, right, EE_EQUAL));
    }
    | equality_expression NE_OP relational_expression
    {
        fprintf(fProductions, "equality_expression: equality_expression NE_OP relational_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_equality_new(left, right, EE_NOT_EQUAL));
    }
    ;

and_expression
    : equality_expression
    {
        fprintf(fProductions, "and_expression: equality_expression \n");
    }
    | and_expression '&' equality_expression
    {
        fprintf(fProductions, "and_expression: and_expression '&' equality_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_BIN_AND));
    }
    ;

exclusive_or_expression
    : and_expression
    {
        fprintf(fProductions, "exclusive_or_expression: and_expression \n");
    }
    | exclusive_or_expression '^' and_expression
    {
        fprintf(fProductions, "exclusive_or_expression: exclusive_or_expression '^' and_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_BIN_XOR));
    }
    ;

inclusive_or_expression
    : exclusive_or_expression
    {
        fprintf(fProductions, "inclusive_or_expression: exclusive_or_expression \n");
    }
    | inclusive_or_expression '|' exclusive_or_expression
    {
        fprintf(fProductions, "inclusive_or_expression: inclusive_or_expression '|' exclusive_or_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_BIN_OR));
    }
    ;

logical_and_expression
    : inclusive_or_expression
    {
        fprintf(fProductions, "logical_and_expression: inclusive_or_expression \n");
    }
    | logical_and_expression AND_OP inclusive_or_expression
    {
        fprintf(fProductions, "logical_and_expression: logical_and_expression AND_OP inclusive_or_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_LOG_AND));
    }
    ;

logical_or_expression
    : logical_and_expression
    {
        fprintf(fProductions, "logical_or_expression: logical_and_expression \n");
    }
    | logical_or_expression OR_OP logical_and_expression
    {
        fprintf(fProductions, "logical_or_expression: logical_or_expression OR_OP logical_and_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_binary_new(left, right, BE_LOG_OR));
    }
    ;

conditional_expression
    : logical_or_expression
    {
        fprintf(fProductions, "conditional_expression: logical_or_expression \n");
    }
    | logical_or_expression '?' expression ':' conditional_expression
    {
        fprintf(fProductions, "conditional_expression: logical_or_expression '?' expression ':' conditional_expression \n");
        not_supported(); /*TODO: */
    }
    ;

assignment_expression
    : conditional_expression
    {
        fprintf(fProductions, "assignment_expression: conditional_expression \n");
    }
    | unary_expression assignment_operator assignment_expression
    {
        fprintf(fProductions, "unary_expression assignment_operator assignment_expression \n");
        ASTnode *right = stack_pop(syntax_stack);
        ASTnode *left = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_assignment_new(left, right, AST_ex_type));
    }
    ;

assignment_operator
    : '='
    {
        AST_ex_type = AE_ASSIGN;
    }
    | MUL_ASSIGN
    {
        AST_ex_type = AE_MUL_ASSIGN;
    }
    | DIV_ASSIGN
    {
        AST_ex_type = AE_DIV_ASSIGN;
    }
    | MOD_ASSIGN
    {
        AST_ex_type = AE_MOD_ASSIGN;
    }
    | ADD_ASSIGN
    {
        AST_ex_type = AE_ADD_ASSIGN;
    }
    | SUB_ASSIGN
    {
        AST_ex_type = AE_SUB_ASSIGN;
    }
    | LEFT_ASSIGN
    {
        AST_ex_type = AE_LEFT_ASSIGN;
    }
    | RIGHT_ASSIGN
    {
        AST_ex_type = AE_RIGHT_ASSIGN;
    }
    | AND_ASSIGN
    {
        AST_ex_type = AE_AND_ASSIGN;
    }
    | XOR_ASSIGN
    {
        AST_ex_type = AE_XOR_ASSIGN;
    }
    | OR_ASSIGN
    {
        AST_ex_type = AE_OR_ASSIGN;
    }
    ;

expression
    : assignment_expression
    {
        fprintf(fProductions, "expression: assignment_expression \n");
    }
    | expression ',' assignment_expression
    {
        fprintf(fProductions, "expression: expression ',' assignment_expression \n");
        not_supported();
    }
    ;

constant_expression
    : conditional_expression
    {
        fprintf(fProductions, "constant_expression: conditional_expression \n");
    }
    ;

declaration
    : declaration_specifiers ';'
    {
        fprintf(fProductions, "declaration : declaration_specifiers ';' \n");
        not_supported();
    }
    | declaration_specifiers init_declarator_list ';'
    {
        fprintf(fProductions, "declaration : declaration_specifiers declaration_specifiers ';' \n");
        ASTnode *declarator_list = stack_pop(syntax_stack);
        ASTnode *specifier = stack_pop(syntax_stack);
        // just set GLOBAL type, we will change this later in function declaration
        stack_push(syntax_stack, ast_declaration_statement_new(specifier, declarator_list, DS_GLOBAL));
    }
    ;

declaration_specifiers
    : storage_class_specifier
    | storage_class_specifier declaration_specifiers {not_supported();}
    | type_specifier
    {
        fprintf(fProductions, "declaration_specifiers : type_specifier ';' \n");
    }
    | type_specifier declaration_specifiers
    {
        fprintf(fProductions, "declaration_specifiers: type_specifier declaration_specifiers \n");
        not_supported();
    }
    | type_qualifier
    {
        fprintf(fProductions, "declaration_specifiers : type_qualifier ';' \n");
        not_supported();
    }
    | type_qualifier declaration_specifiers {not_supported();}
    | function_specifier
    {
        fprintf(fProductions, "declaration_specifiers: function_specifier \n");
    }
    | function_specifier declaration_specifiers
    {
        fprintf(fProductions, "declaration_specifiers: function_specifier declaration_specifiers \n");
        not_supported();
    }
    ;

init_declarator_list
    : init_declarator
    {
        fprintf(fProductions, "init_declarator_list: init_declarator \n");
    }
    | init_declarator_list ',' init_declarator
    {
        fprintf(fProductions, "init_declarator_list: init_declarator_list ',' init_declarator \n");
        not_supported();
    }
    ;

init_declarator
    : declarator
    {
        fprintf(fProductions, "init_declarator: declarator \n");
        ASTnode *declarator = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_init_declarator_new(declarator, NULL, ID_NOINIT));
    }
    | declarator '=' initializer
    {
        fprintf(fProductions, "init_declarator: declarator '=' initializer \n");
        ASTnode *initializer = stack_pop(syntax_stack);
        ASTnode *declarator = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_init_declarator_new(declarator, initializer, ID_INIT));
    }
    ;

storage_class_specifier
    : TYPEDEF
    | EXTERN
    | STATIC
    | AUTO
    | REGISTER
    ;

type_specifier
    : VOID {stack_push(syntax_stack, ast_type_spec_new(TS_VOID));}
    | CHAR {stack_push(syntax_stack, ast_type_spec_new(TS_CHAR));}
    | SHORT {stack_push(syntax_stack, ast_type_spec_new(TS_SHORT));}
    | INT {stack_push(syntax_stack, ast_type_spec_new(TS_INT));}
    | LONG {stack_push(syntax_stack, ast_type_spec_new(TS_INT));}
    | FLOAT {stack_push(syntax_stack, ast_type_spec_new(TS_LONG));}
    | DOUBLE {stack_push(syntax_stack, ast_type_spec_new(TS_FLOAT));}
    | SIGNED {stack_push(syntax_stack, ast_type_spec_new(TS_SIGNED));}
    | UNSIGNED {stack_push(syntax_stack, ast_type_spec_new(TS_UNSIGNED));}
    | BOOL {stack_push(syntax_stack, ast_type_spec_new(TS_BOOL));}
    | COMPLEX {stack_push(syntax_stack, ast_type_spec_new(TS_COMPLEX));}
    | IMAGINARY {stack_push(syntax_stack, ast_type_spec_new(TS_IMAGINARY));}
    | struct_or_union_specifier
    | enum_specifier
    | TYPE_NAME
    ;

struct_or_union_specifier
    : struct_or_union IDENTIFIER '{' struct_declaration_list '}' {not_supported();}
    | struct_or_union '{' struct_declaration_list '}' {not_supported();}
    | struct_or_union IDENTIFIER {not_supported();}
    ;

struct_or_union
    : STRUCT {not_supported();}
    | UNION {not_supported();}
    ;

struct_declaration_list
    : struct_declaration
    | struct_declaration_list struct_declaration {not_supported();}
    ;

struct_declaration
    : specifier_qualifier_list struct_declarator_list ';' {not_supported();}
    ;

specifier_qualifier_list
    : type_specifier specifier_qualifier_list
    | type_specifier
    | type_qualifier specifier_qualifier_list
    | type_qualifier
    ;

struct_declarator_list
    : struct_declarator
    | struct_declarator_list ',' struct_declarator {not_supported();}
    ;

struct_declarator
    : declarator
    | ':' constant_expression {not_supported();}
    | declarator ':' constant_expression {not_supported();}
    ;

enum_specifier
    : ENUM '{' enumerator_list '}'  {not_supported();}
    | ENUM IDENTIFIER '{' enumerator_list '}'  {not_supported();}
    | ENUM '{' enumerator_list ',' '}'  {not_supported();}
    | ENUM IDENTIFIER '{' enumerator_list ',' '}'  {not_supported();}
    | ENUM IDENTIFIER {not_supported();}
    ;

enumerator_list
    : enumerator {fprintf(fProductions, "enumerator_list: enumerator \n");}
    | enumerator_list ',' enumerator  {not_supported();}
    ;

enumerator
    : IDENTIFIER {fprintf(fProductions, "enumerator: IDENTIFIER %s \n", $1);}
    | IDENTIFIER '=' constant_expression  {not_supported();}
    ;

type_qualifier
    : CONST  {not_supported();}
    | RESTRICT  {not_supported();}
    | VOLATILE  {not_supported();}
    ;

function_specifier
    : INLINE  {not_supported();}
    ;

declarator
    : pointer direct_declarator
    {
        fprintf(fProductions, "declarator : pointer direct_declarator \n");
        not_supported();
    }
    | direct_declarator
    {
        fprintf(fProductions, "declarator : direct_declarator \n");
    }
    ;


direct_declarator
    : IDENTIFIER
    {
        fprintf(fProductions, "direct_declarator : IDENTIFIER: %s \n", $1);
        stack_push(syntax_stack, ast_identifier_new((char*)$1));
    }
    | '(' declarator ')'
    {
        fprintf(fProductions, "direct_declarator : '(' declarator ')''");
        //not_supported();
    }
    | direct_declarator '[' type_qualifier_list assignment_expression ']'  {not_supported();}
    | direct_declarator '[' type_qualifier_list ']'  {not_supported();} {not_supported();}
    | direct_declarator '[' assignment_expression ']'  {not_supported();} {not_supported();}
    | direct_declarator '[' STATIC type_qualifier_list assignment_expression ']'  {not_supported();}
    | direct_declarator '[' type_qualifier_list STATIC assignment_expression ']'  {not_supported();}
    | direct_declarator '[' type_qualifier_list '*' ']'  {not_supported();}
    | direct_declarator '[' '*' ']'  {not_supported();}
    | direct_declarator '[' ']'  {not_supported();}
    | direct_declarator '(' parameter_type_list ')'
    {
        fprintf(fProductions, "direct_declarator : direct_declarator (%s) '(' parameter_type_list ')'\n", $1);
        
        // pop parameters from stack:
        ASTnode *parameters = stack_pop(syntax_stack); // this is parameter list
        //pop direct_declarator from stack (this is function name)
        ASTnode *name = stack_pop(syntax_stack);
        
        //the popped AST node should be identifier:
        assert (name->type == AST_IDENTIFIER);
        
        //create a new function declarator:
        stack_push(syntax_stack, ast_function_declarator_new(name, parameters, FD_ARGS));
        
    }
    | direct_declarator '(' identifier_list ')'
    {
        fprintf(fProductions, "direct_declarator : direct_declarator '(' identifier_list ')'\n");
        not_supported();
    }
    | direct_declarator '(' ')'
    {
        fprintf(fProductions, "direct_declarator : direct_declarator '(' ')' \n");
        //not_supported();
        
        //pop direct_declarator from stack (this is function name)
        ASTnode *name = stack_pop(syntax_stack);
        //the popped AST node should be identifier:
        assert (name->type == AST_IDENTIFIER);
        //create a new function declarator:
        stack_push(syntax_stack, ast_function_declarator_new(name, NULL, FD_NOARGS));
        
    }
    ;

pointer
    : '*' {not_supported();}
    | '*' type_qualifier_list {not_supported();}
    | '*' pointer {not_supported();}
    | '*' type_qualifier_list pointer {not_supported();}
    ;

type_qualifier_list
    : type_qualifier {fprintf(fProductions, "type_qualifier_list: type_qualifier \n");}
    | type_qualifier_list type_qualifier {not_supported();}
    ;


parameter_type_list
    : parameter_list
    {
        fprintf(fProductions, "parameter_type_list: parameter_list\n");
    }
    | parameter_list ',' ELLIPSIS {not_supported();}
    ;

parameter_list
    : parameter_declaration
    {
        fprintf(fProductions, "parameter_list: parameter_declaration\n");
        
        
        assert(!stack_empty(syntax_stack));
        // Pop the parameter declaration :
        ASTnode *param_decl = stack_pop(syntax_stack);
        
        //create a new parameter list:
        ASTnode *ast_pl = ast_parameter_list_new(list_new());

        /*
         * Now, add the parameter declaration to the begining of the list
         * (PUSH statement) and push the new list onto stack
         */
        
        list_push(ast_pl->parameter_list->parameter_declarations, param_decl);
        stack_push(syntax_stack, ast_pl);
        
    }
    | parameter_list ',' parameter_declaration
    {
        fprintf(fProductions, "parameter_list: parameter_list ',' parameter_declaration\n");
        
        
        assert(!stack_empty(syntax_stack));
        // Pop the parameter declaration :
        ASTnode *param_decl = stack_pop(syntax_stack);
        
        // now pop the parameter list from the stack:
        ASTnode *ast_pl = stack_pop(syntax_stack);

        /*
        * Now, add the parameter declaration to the begining of the list
        * (PUSH statement) and push the list onto stack
        */
        
        list_push(ast_pl->parameter_list->parameter_declarations, param_decl);
        stack_push(syntax_stack, ast_pl);
    }
    ;

parameter_declaration
    : declaration_specifiers declarator
    {
        fprintf(fProductions, "parameter_declaration: declaration_specifiers declarator \n");
        
        ASTnode *declarator = stack_pop(syntax_stack);
        ASTnode *specifier = stack_pop(syntax_stack);
        // create a new parameter declaration:
        // as this is function parameter declaration, set DS_ARGUMENTS type:
        stack_push(syntax_stack, ast_parameter_declaration_new(specifier, declarator, DS_ARGUMENTS));
    }
    | declaration_specifiers abstract_declarator {not_supported();}
    | declaration_specifiers
    {
        fprintf(fProductions, "parameter_declaration: declaration_specifiers \n");
        not_supported();
    }
    ;

identifier_list
    : IDENTIFIER {fprintf(fProductions, "identifier_list: IDENTIFIER %s \n", $1);}
    | identifier_list ',' IDENTIFIER {not_supported();}
    ;

type_name
    : specifier_qualifier_list {fprintf(fProductions, "type_name: specifier_qualifier_list \n");}
    | specifier_qualifier_list abstract_declarator {not_supported();}
    ;

abstract_declarator
    : pointer {not_supported();}
    | direct_abstract_declarator {not_supported();}
    | pointer direct_abstract_declarator {not_supported();}
    ;

direct_abstract_declarator
    : '(' abstract_declarator ')' {not_supported();}
    | '[' ']' {not_supported();}
    | '[' assignment_expression ']' {not_supported();}
    | direct_abstract_declarator '[' ']' {not_supported();}
    | direct_abstract_declarator '[' assignment_expression ']' {not_supported();}
    | '[' '*' ']' {not_supported();}
    | direct_abstract_declarator '[' '*' ']' {not_supported();}
    | '(' ')' {not_supported();}
    | '(' parameter_type_list ')'
    {
        fprintf(fProductions, "direct_abstract_declarator: '(' parameter_type_list ')'\n");
        not_supported();
        
    }
    | direct_abstract_declarator '(' ')' {not_supported();}
    | direct_abstract_declarator '(' parameter_type_list ')'
    {
        fprintf(fProductions, "direct_abstract_declarator: direct_abstract_declarator '(' parameter_type_list ')'\n");
        not_supported();
    }
    ;

initializer
    : assignment_expression {fprintf(fProductions, "initializer: assignment_expression \n");}
    | '{' initializer_list '}' {not_supported();}
    | '{' initializer_list ',' '}' {not_supported();}
    ;

initializer_list
    : initializer {fprintf(fProductions, "initializer_list: initializer \n");}
    | designation initializer {not_supported();}
    | initializer_list ',' initializer {not_supported();}
    | initializer_list ',' designation initializer {not_supported();}
    ;

designation
    : designator_list '=' {not_supported();}
    ;

designator_list
    : designator
    | designator_list designator {not_supported();}
    ;

designator
    : '[' constant_expression ']' {not_supported();}
    | '.' IDENTIFIER {not_supported();}
    ;

statement
    : labeled_statement {fprintf(fProductions, "statement: labeled_statement \n");}
    | compound_statement {fprintf(fProductions, "statement: compound_statement \n");}
    | expression_statement {fprintf(fProductions, "statement: expression_statement \n");}
    | selection_statement {fprintf(fProductions, "statement: selection_statement \n");}
    | iteration_statement {fprintf(fProductions, "statement: iteration_statement \n");}
    | jump_statement {fprintf(fProductions, "statement: jump_statement \n");}
    ;

labeled_statement
    : IDENTIFIER ':' statement  {not_supported(); /*TODO: labes*/}
    | CASE constant_expression ':' statement {not_supported();}
    | DEFAULT ':' statement {not_supported();}
    ;

compound_statement
    : '{' '}'
    {
        fprintf(fProductions, "compound_statement: '{' '}' \n");
        not_supported();
    }
    | '{' block_item_list '}'
    {
        fprintf(fProductions, "compound_statement: '{' block_item_list '}' \n");
        // pop the block, create a new compound statement and push it
        ASTnode *block = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_compound_statement_new(block));
        
        
        ASTnode *compound_statement = stack_pop(syntax_stack);
        stack_push(syntax_stack,compound_statement);
    }
    ;

block_item_list
    : block_item
    {
        fprintf(fProductions, "block_item_list: block_item \n");
        assert(!stack_empty(syntax_stack));
        // Pop the block item (statement or declaration):
        ASTnode *block_item = stack_pop(syntax_stack);
        
        //create a new block:
        ASTnode *ast_block = ast_block_new(list_new());
        number_of_blocks++;

        /*
         * Now, add the block item to the begining of the list
         * (PUSH statement) and push the new block onto stack
         */
        
        list_push(ast_block->block->statements, block_item);
        stack_push(syntax_stack, ast_block);
    }
    | block_item_list block_item
    {
        fprintf(fProductions, "block_item_list: block_item_list block_item \n");
        assert(!stack_empty(syntax_stack));
        // Top of the stack contains a block item, which is a statement/declaration. POP it.
        ASTnode *block_item = stack_pop(syntax_stack);
        
        // now pop the block from the stack:
        ASTnode *ast_block = stack_pop(syntax_stack);

        /*
         * Now, add the new statement from top of the stack to the begining of the list
         * (PUSH statement) and push the new block onto stack
         */
        
        list_push(ast_block->block->statements, block_item);
        stack_push(syntax_stack, ast_block);
    }
    ;

block_item
    : declaration
    {
        fprintf(fProductions, "block_item: declaration \n");
    }
    | statement
    {
        fprintf(fProductions, "block_item: statement \n");
    }
    ;

expression_statement
    : ';'
    | expression ';'
    {
        fprintf(fProductions, "expression statement: expression ; \n");
        ASTnode *current_ast = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_expression_statement_new(current_ast, ES_EXPRESSION));
    }
    ;

selection_statement
    : IF '(' expression ')' statement
    {
        fprintf(fProductions, "selection_statement: IF '(' expression ')' statement \n");
        ASTnode *t_statement = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_selection_statement_new(condition,t_statement,NULL,SS_IF));
    }
    | IF '(' expression ')' statement ELSE statement
    {
        fprintf(fProductions, "selection_statement: IF '(' expression ')' statement ELSE statement \n");
        ASTnode *f_statement = stack_pop(syntax_stack);
        ASTnode *t_statement = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_selection_statement_new(condition,t_statement,f_statement,SS_IFELSE));
    }
    | SWITCH '(' expression ')' statement
    {
        fprintf(fProductions, "selection_statement: SWITCH '(' expression ')' statement \n");
        ASTnode *t_statement = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_selection_statement_new(condition,t_statement,NULL,SS_SWITCH));
    }
    ;

iteration_statement
    : WHILE '(' expression ')' statement
    {
        fprintf(fProductions, "iteration_statement: WHILE '(' expression ')' statement \n");
        ASTnode *statement = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_iteration_statement_new(condition,statement,NULL,NULL,IS_WHILE));
    }
    | DO statement WHILE '(' expression ')' ';'
    {
        fprintf(fProductions, "iteration_statement: DO statement WHILE '(' expression ')' ';' \n");
        ASTnode *statement = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_iteration_statement_new(condition,statement,NULL,NULL,IS_DO));
    }
    | FOR '(' expression_statement expression_statement ')' statement {not_supported();}
    | FOR '(' expression_statement expression_statement expression ')' statement
    {
        fprintf(fProductions, "iteration_statement: FOR '(' expression_statement expression_statement expression ')' statement \n");
        ASTnode *statement = stack_pop(syntax_stack);
        ASTnode *forupdate = stack_pop(syntax_stack);
        ASTnode *condition = stack_pop(syntax_stack);
        ASTnode *forinit = stack_pop(syntax_stack);
        
        stack_push(syntax_stack, ast_iteration_statement_new(condition,statement,forinit,forupdate,IS_FOR));
        
    }
    | FOR '(' declaration expression_statement ')' statement {not_supported();}
    | FOR '(' declaration expression_statement expression ')' statement {not_supported();}
    ;

jump_statement
    : GOTO IDENTIFIER ';'
    | CONTINUE ';'
    | BREAK ';'
    | RETURN ';'
    | RETURN expression ';'
    {
        fprintf(fProductions, "jump_statement: RETURN expression ; \n");
        ASTnode *current_ast = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_jump_statement_new(current_ast, JS_RETURN_EXP));
    }
    ;

translation_unit
    : external_declaration
    {
        fprintf(fProductions, "translation_unit: external_declaration\n");
        assert(!stack_empty(syntax_stack));
        // Pop the external_declaration (declaration statement or function definition):
        ASTnode *ed = stack_pop(syntax_stack);
        
        //create a new translational unit:
        ASTnode *ast_tu = ast_translational_unit_new(list_new());

        /*
         * Now, add the external declaration to the begining of the list
         * (PUSH statement) and push the translational unit onto stack
         */
        
        list_push(ast_tu->translational_unit->ext_declarations, ed);
        stack_push(syntax_stack, ast_tu);
    }
    | translation_unit external_declaration
    {
        fprintf(fProductions, "translation_unit: translation_unit external_declaration\n");
        assert(!stack_empty(syntax_stack));
        // Pop the external_declaration (declaration statement or function definition):
        ASTnode *ed = stack_pop(syntax_stack);
        
        //pop the translational unit from the stack:
        ASTnode *ast_tu = stack_pop(syntax_stack);

        /*
         * Now, add the external declaration to the begining of the list
         * (PUSH statement) and push the translational unit onto stack
         */
        
        list_push(ast_tu->translational_unit->ext_declarations, ed);
        stack_push(syntax_stack, ast_tu);
    }
    ;

external_declaration
    : function_definition
    {
        fprintf(fProductions, "external_declaration: function_definition\n");
    }
    | declaration
    {
        fprintf(fProductions, "external_declaration: declaration\n");
    }
    ;

function_definition
    : declaration_specifiers declarator declaration_list compound_statement
    {
        fprintf(fProductions, "function_definition: declaration_specifiers declarator declaration_list compound_statement\n");
        not_supported();
        /*
        char* funcname = $2;
        
        if (symbol_lookup_in_scope(symtab, current_scope, funcname) == false){
            // add new function name:
            
            Symbol* sym = func_symbol_new(current_scope, funcname, ST_VOID);
            symbol_insert(symtab, sym);
            
        }
        
        ASTnode *compound_statement = stack_pop(syntax_stack);
        // find all declarations in the compound statement and change their type to local
        ASTBlock* block = compound_statement->compound_statement->block->block;
        List *statements = block->statements;
        ASTnode *statement;
        
        for (int i = 0; i < list_length(statements); i++) {
            statement = list_get(statements, i);
            if (statement->type == AST_DECLARATION_STATEMENT) {
                statement->declaration_statement->type = DS_LOCAL;
            }
        }
        
        ASTnode *ast_declaration_list = stack_pop(syntax_stack);
        // find all declarations in the compound statement and change their type to local
        
        List *decl_statements = ast_declaration_list->declaration_list->declaration_statements;
        ASTnode *decl_statement;
        for (int i = 0; i < list_length(decl_statements); i++) {
            decl_statement = list_get(statements, i);
            if (decl_statement->type == AST_DECLARATION_STATEMENT) {
                decl_statement->declaration_statement->type = DS_ARGUMENTS;
            }
        }
        
        ASTnode *declarator = stack_pop(syntax_stack);
        ASTnode *declaration_specifiers = stack_pop(syntax_stack);
        stack_push(syntax_stack, ast_function_definition_new(declaration_specifiers, declarator , compound_statement));
         */
        
    }
    | declaration_specifiers declarator compound_statement
    {
        fprintf(fProductions, "function_definition: declaration_specifiers declarator compound_statement \n");
        
        ASTnode *compound_statement = stack_pop(syntax_stack);
        
        /* find all declarations in the compound statement and change their type to local */
        ASTBlock* block = compound_statement->compound_statement->block->block;
        List *statements = block->statements;
        ASTnode *statement;
        
        for (int i = 0; i < list_length(statements); i++) {
            statement = list_get(statements, i);
            if (statement->type == AST_DECLARATION_STATEMENT) {
                statement->declaration_statement->type = DS_LOCAL;
            }
        }
        
        // POP declarator from stack:
        ASTnode *declarator = stack_pop(syntax_stack);
    
        assert(declarator->type == AST_FUNCTION_DECLARATOR);
        assert(declarator->function_declarator->func_identifier->type == AST_IDENTIFIER);
        char* funcname = declarator->function_declarator->func_identifier->identifier->identifier_name;
        
        if (symbol_lookup_in_scope(symtab, current_scope, funcname, ST_GLOBAL) == false){
            // add new function name:
            Symbol* sym = func_symbol_new(current_scope, funcname, ST_VOID);
            symbol_insert(symtab, sym);
        }
        
        // POP function type specifier:
        ASTnode *declaration_specifiers = stack_pop(syntax_stack);
        
        // now pusc a new function definition:
        stack_push(syntax_stack, ast_function_definition_new(declaration_specifiers, declarator , compound_statement));
    }
    ;

declaration_list
    : declaration
    {
        fprintf(fProductions, "declaration_list: declaration\n" );
        not_supported();
        /* TODO: 
        assert(!stack_empty(syntax_stack));
        // Pop the declaration :
        ASTnode *declaration = stack_pop(syntax_stack);
        
        //create a new declaration list:
        ASTnode *ast_dl = ast_declaration_list_new(list_new());
        
        //number_of_blocks++;
        
        /*
         * Now, add the declaration to the begining of the list
         * (PUSH statement) and push the new declaration list onto stack
         */
        /*
        list_push(ast_dl->declaration_list->declaration_statements, declaration);
        stack_push(syntax_stack, ast_dl);
        
        */
    }
    | declaration_list declaration
    {
        fprintf(fProductions, "declaration_list: declaration declaration\n");
        not_supported();
    }
    ;


%%

#include <stdio.h>
/*
extern char yytext[];
extern int column;

void yyerror(char const *s)
{
    fflush(stdout);
    printf("\n%*s\n%*s\n", column, "^", column, s);
}
 */

